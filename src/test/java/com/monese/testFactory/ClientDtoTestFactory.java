package com.monese.testFactory;

import com.monese.api.dto.ClientDto;
import com.monese.model.Client;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import static com.monese.api.dto.ClientDto.ClientStateDto.ACTIVE;

/**
 * @author acozma
 */
public class ClientDtoTestFactory {

	private static final String S_2019_11_03_15_30_45 = DateTimeFormatter.ISO_DATE_TIME
			.format(LocalDateTime.of(2019, 11, 3, 15, 30, 45));

	/**
	 * Create test {@link Client} entity
	 *
	 * @param id
	 *
	 * @return client entity
	 */
	public static ClientDto createClientDto(final Long id) {

		return createClientDto(id, "Sherlock Holmes", "221B Backer Street");
	}

	public static ClientDto createClientDto(final Long id, final String name, final String address) {

		ClientDto testClient = new ClientDto();
		testClient.setClientId(id);
		testClient.setName(name);
		testClient.setAddress(address);
		testClient.setNumericPersonalCode("19272183126");
		testClient.setPhoneNumber("0747356347");
		testClient.setAccountIds(new ArrayList<>());
		testClient.setState(ACTIVE);
		testClient.setCreationDate(S_2019_11_03_15_30_45);
		testClient.setLastModifiedDate(S_2019_11_03_15_30_45);

		return testClient;
	}
}
