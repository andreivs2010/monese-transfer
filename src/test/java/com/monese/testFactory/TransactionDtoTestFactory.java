package com.monese.testFactory;

import com.monese.api.dto.TransactionDto;

import java.math.BigDecimal;

import static com.monese.api.dto.TransactionDto.TransactionTypeDto.TRANSFER;
import static java.time.LocalDateTime.of;
import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;

/**
 * @author acozma
 */
public class TransactionDtoTestFactory {

	private static final String S_2019_11_03_14_30_22 = ISO_DATE_TIME.format(of(2019, 11, 3, 14, 30, 22));
	private static final BigDecimal BD_10_00 = new BigDecimal("10.00");

	public static TransactionDto createTransactionDto() {

		return createTransactionDto(1L, 2L, 3L, 4L);
	}

	public static TransactionDto createTransactionDto(final Long transactionId, final Long parentAccountId,
													  final Long parentTrasactionLinkId, final Long childTransactionLinkId) {

		TransactionDto dto = new TransactionDto();
		dto.setAmount(BD_10_00);
		dto.setCreationDate(S_2019_11_03_14_30_22);
		dto.setParentAccountId(parentAccountId);
		dto.setType(TRANSFER);
		dto.setParentTransactionLinkId(parentTrasactionLinkId);
		dto.setChildTransactionLinkId(childTransactionLinkId);
		dto.setTransactionId(transactionId);
		return dto;
	}
}
