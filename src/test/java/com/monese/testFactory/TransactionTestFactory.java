package com.monese.testFactory;

import com.monese.model.Account;
import com.monese.model.Transaction;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static com.monese.model.Transaction.TransactionType.TRANSFER;

/**
 * @author acozma
 */
public class TransactionTestFactory {

	private static final LocalDateTime LDT_2019_11_03_14_30_22 = LocalDateTime.of(2019, 11, 3, 14, 30, 22);
	private static final BigDecimal BD_10_00 = new BigDecimal("10.00");

	public static Transaction createTransaction() {

		return createTransaction(1L, null, 3L, 4L);
	}

	public static Transaction createTransaction(final Long transactionId, final Account parentAccount,
												final Long parentTransactionLinkId, final Long childTransactionLinkId) {

		Transaction transaction = new Transaction();
		transaction.setTransactionId(transactionId);
		transaction.setAmount(BD_10_00);
		transaction.setCreationDate(LDT_2019_11_03_14_30_22);
		transaction.setParentAccount(parentAccount);
		transaction.setType(TRANSFER);
		transaction.setParentTransactionLinkId(parentTransactionLinkId);
		transaction.setChildTransactionLinkId(childTransactionLinkId);
		return transaction;
	}
}
