package com.monese.testFactory;

import com.monese.model.Account;
import com.monese.model.Client;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static com.monese.model.Account.AccountState.INACTIVE;

/**
 * @author acozma
 */
public class AccountTestFactory {

	private static final LocalDateTime LDT_2019_11_03_14_30_22 = LocalDateTime.of(2019, 11, 3, 14, 30, 22);
	private static final BigDecimal BD_100_00 = new BigDecimal("100.00");

	public static Account createAccount() {

		Account account = new Account();

		account.setBalance(BD_100_00);
		account.setCreationDate(LDT_2019_11_03_14_30_22);
		account.setLastModifiedDate(LDT_2019_11_03_14_30_22);
		account.setState(INACTIVE);
		account.setTransactions(new ArrayList<>());

		return account;
	}

	public static Account createAccount(Client accountHolder) {

		Account account = new Account();

		account.setAccountHolder(accountHolder);
		account.setBalance(BD_100_00);
		account.setCreationDate(LDT_2019_11_03_14_30_22);
		account.setLastModifiedDate(LDT_2019_11_03_14_30_22);
		account.setState(INACTIVE);
		account.setTransactions(new ArrayList<>());

		return account;
	}
}
