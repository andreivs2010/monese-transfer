package com.monese.testFactory;

import com.monese.model.Client;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static com.monese.model.Client.ClientState.ACTIVE;

/**
 * @author acozma
 */
public class ClientTestFactory {

	private static final LocalDateTime LDT_2019_11_03_15_30_45 = LocalDateTime.of(2019, 11, 3, 15, 30, 45);

	/**
	 * Create test {@link Client} entity
	 *
	 * @return client entity
	 */
	public static Client createClient() {

		return createClient("Sherlock Holmes", "221B Backer Street");
	}

	public static Client createClient(final String name, final String address) {

		Client testClient = new Client();
		testClient.setName(name);
		testClient.setAddress(address);
		testClient.setNumericPersonalCode("19272183126");
		testClient.setPhoneNumber("0747356347");
		testClient.setAccounts(new ArrayList<>());
		testClient.setState(ACTIVE);
		testClient.setCreationDate(LDT_2019_11_03_15_30_45);
		testClient.setLastModifiedDate(LDT_2019_11_03_15_30_45);

		return testClient;
	}

	public static Client createClient(final String name) {

		return createClient(name, "221B Backer Street");
	}
}
