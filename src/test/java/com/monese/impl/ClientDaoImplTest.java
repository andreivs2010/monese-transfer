package com.monese.impl;

import com.monese.dao.EntityDao;
import com.monese.dao.exception.ClientNotFoundException;
import com.monese.dao.impl.ClientDaoImpl;
import com.monese.model.Account;
import com.monese.model.Client;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.monese.model.Client.ClientState.INACTIVE;
import static com.monese.testFactory.ClientTestFactory.createClient;
import static org.junit.Assert.*;

/**
 * Test class for {@link ClientDaoImpl}
 *
 * @author acozma
 */
public class ClientDaoImplTest extends AbstractEntityDaoImplTest<Client> {

    @Override
    protected EntityDao<Client> createDao() {

        return ClientDaoImpl.getInstance();
    }

    @Override
    protected Client createEntity() {

        return createClient();
    }

    @Test
    public void givenExistingClientWhenModifyValueAndStoreThenValueIsUpdated() {

        //setup
        Client testClient = createEntity();
        dao.storeEntity(testClient);
        testClient.setState(INACTIVE);

        //execute
        testClient = dao.updateEntity(testClient);

        //verify
        Client clientFromPersistenceUnit = dao.getEntity(testClient.getId());

        assertEquals(INACTIVE, clientFromPersistenceUnit.getState());
    }

    @Test
    public void givenExistingClientWhenGetByIdThenReturnClient() {

        //setup
        Client testClient = createEntity();
        dao.storeEntity(testClient);

        //execute
        Client retrievedClient = dao.getEntity(testClient.getId());

        //verify
        assertEqualClients(testClient, retrievedClient);
    }

    private void assertEqualClients(final Client testClient, final Client retrievedClient) {

        assertEquals(testClient.getId(), retrievedClient.getId());
        assertEquals(testClient.getName(), retrievedClient.getName());
        assertEquals(testClient.getState(), retrievedClient.getState());
        assertEquals(testClient.getAddress(), retrievedClient.getAddress());
        assertEquals(testClient.getCreationDate(), retrievedClient.getCreationDate());
        assertEquals(testClient.getNumericPersonalCode(), retrievedClient.getNumericPersonalCode());
        assertEquals(testClient.getLastModifiedDate(), retrievedClient.getLastModifiedDate());
        assertEquals(testClient.getPhoneNumber(), retrievedClient.getPhoneNumber());

        assertEqualAccounts(testClient, retrievedClient);
    }

    private void assertEqualAccounts(final Client testClient, final Client retrievedClient) {

        List<Account> accounts = testClient.getAccounts();
        List<Account> retrievedAccounts = retrievedClient.getAccounts();
        for (int i = 0; i < accounts.size(); i++) {

            Account account = accounts.get(i);
            Account retrievedAccount = retrievedAccounts.get(i);
            assertEquals(account.getId(), retrievedAccount.getId());
        }
    }

    @Test
    public void givenMultipleClientsWhenGetByIdsThenReturnCorrectClients() {

        //setup
        Client testClient1 = createClientWith("C1", "A1");
        Client testClient2 = createClientWith("C2", "A2");
        Client testClient3 = createClientWith("C3", "A3");
        Client testClient4 = createClientWith("C4", "A4");

        dao.storeEntity(testClient1);
        dao.storeEntity(testClient2);
        dao.storeEntity(testClient3);
        dao.storeEntity(testClient4);

        //execute
        List<Long> fetchedIds = Arrays.asList(2L, 4L);
        List<Client> fetchedEntities = dao.getEntities(fetchedIds);

        assertEquals(2, fetchedEntities.size());
        assertEqualClients(testClient2, fetchedEntities.get(0));
        assertEqualClients(testClient4, fetchedEntities.get(1));

    }

    private Client createClientWith(final String name, final String address) {

        return createClient(name, address);
    }

    @Test
    public void givenClientIdWhenDeleteNotExistingClientThenExpectException() {

        //setup
        Long clientId = 5L;

        thrown.expect(ClientNotFoundException.class);

        //execute
        dao.delete(clientId);
    }

    @Test
    public void givenClientIdWhenDeleteExistingClientThenCheckRemovedEntity() {

        //setup
        Client testClient = createEntity();

        dao.storeEntity(testClient);

        //execute
        Long id = testClient.getId();
        dao.delete(id);

        //verify
        Client client = dao.getEntity(id);
        assertNull(client);
    }

    @Test
    public void givenEntityWhenStoreThenCheckThatEntityWasSuccessfullyStored() {

        //setup
        Client entity = createEntity();

        //execute
        dao.storeEntity(entity);

        //verify
        Client entityFromPersistenceUnit = dao.getEntity(entity.getId());

        assertNotNull(entityFromPersistenceUnit);
    }
}