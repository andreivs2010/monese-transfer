package com.monese.impl;

import com.monese.BaseTestCase;
import com.monese.EntityManagerUtils;
import com.monese.dao.EntityDao;
import com.monese.model.HasId;
import org.junit.After;
import org.junit.Before;

/**
 * Base test class for {@link com.monese.dao.impl.AbstractEntityDaoImpl}
 *
 * @author acozma
 */
public abstract class AbstractEntityDaoImplTest<E extends HasId> extends BaseTestCase {

    EntityDao<E> dao;

    @Before
    public void setUp() {

        dao = createDao();
    }

    @After
    public void tearDown() {

        EntityManagerUtils.closeEntityManagerFactory();
    }

    /**
     * Get the dao implementation service
     *
     * @return the dao implementation service
     */
    protected abstract EntityDao<E> createDao();

    /**
     * Create mock entity used for testing
     *
     * @return entity
     */
    protected abstract E createEntity();
}