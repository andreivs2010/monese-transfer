package com.monese.impl;

import com.monese.dao.EntityDao;
import com.monese.dao.exception.AccountNotFoundException;
import com.monese.dao.impl.AccountDaoImpl;
import com.monese.dao.impl.ClientDaoImpl;
import com.monese.dao.impl.TransactionDaoImpl;
import com.monese.model.Account;
import com.monese.model.Client;
import com.monese.model.Transaction;
import com.monese.testFactory.AccountTestFactory;
import com.monese.testFactory.ClientTestFactory;
import com.monese.testFactory.TransactionTestFactory;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Test class for {@link AccountDaoImpl}
 *
 * @author acozma
 */
public class AccountDaoImplTest extends AbstractEntityDaoImplTest<Account> {

    @Override
    protected EntityDao<Account> createDao() {

        return AccountDaoImpl.getInstance();
    }

    @Override
    protected Account createEntity() {

        return AccountTestFactory.createAccount();
    }

    private Transaction createTransaction(final Long transactionId, final Account parentAccount) {

        return TransactionTestFactory.createTransaction(transactionId, parentAccount, 3L,
                4L);
    }

    private Client createClient() {

        return ClientTestFactory.createClient();
    }

    @Test
    public void givenNewAccountWhenStoreThenCheckEntityCreated() {

        // setup
        Client accountHolder = createAndPersistClient();

        Account entity = createEntity();
        entity.setAccountHolder(accountHolder);

        // execute
        dao.storeEntity(entity);

        // verify
        Long id = entity.getId();
        Account entityFromDb = dao.getEntity(id);
        assertNotNull(entityFromDb);

        assertEqualAccounts(entity, entityFromDb);

        List<Transaction> transactions = entityFromDb.getTransactions();
        assertTrue(transactions.isEmpty());
    }

    private void assertEqualAccounts(final Account entity, final Account entityFromDb) {

        assertEquals(entity.getBalance(), entityFromDb.getBalance());
        assertEquals(entity.getCreationDate(), entityFromDb.getCreationDate());
        assertEquals(entity.getLastModifiedDate(), entityFromDb.getLastModifiedDate());
        assertEquals(entity.getState(), entityFromDb.getState());

        assertEqualAccountHolder(entity, entityFromDb);

        assertTransactionsEqual(entity.getTransactions(), entityFromDb.getTransactions());
    }

    private void assertEqualAccountHolder(final Account entity, final Account entityFromDb) {

        assertEquals(entity.getAccountHolder().getId(), entityFromDb.getAccountHolder().getId());
    }

    private Client createAndPersistClient() {

        Client accountHolder = createClient();
        ClientDaoImpl.getInstance().storeEntity(accountHolder);
        return accountHolder;
    }

    @Test
    public void givenExistingAccountWithTransactionWhenGetThenCheckEarlyFetch() {

        // setup
        Client accountHolder = createAndPersistClient();

        Account entity = createEntity();
        entity.setAccountHolder(accountHolder);

        dao.storeEntity(entity);

        List<Transaction> transactions = createPersistedTransactions(entity);
        entity.setTransactions(transactions);
        dao.updateEntity(entity);

        // execute
        Long id = entity.getId();
        Account entityFromDb = dao.getEntity(id);

        // verify
        assertNotNull(entityFromDb);

        assertEqualAccounts(entity, entityFromDb);
    }

    private void assertTransactionsEqual(final List<Transaction> transactions,
                                         final List<Transaction> transactionsFromDb) {

        for (int i = 0; i < transactions.size(); i++) {

            assertEquals(transactions.get(i).getId(), transactionsFromDb.get(i).getId());
        }
    }

    private List<Transaction> createPersistedTransactions(final Account entity) {

        List<Transaction> transactions = new ArrayList<>();

        Transaction transaction = createAndPersistTransaction(entity);
        transactions.add(transaction);

        transaction = createAndPersistTransaction(entity);
        transactions.add(transaction);
        return transactions;
    }

    private Transaction createAndPersistTransaction(final Account parentAccount) {

        Transaction transaction = createTransaction(null, parentAccount);
        TransactionDaoImpl.getInstance().storeEntity(transaction);
        return transaction;
    }

    @Test
    public void givenMultipleAccountsWhenGetByIdsThenReturnCorrectValues() {

        // setup
        Account acc1 = createAndPersistAccountFor("c1");
        createAndPersistAccountFor("c2");
        Account acc3 = createAndPersistAccountFor("c3");
        createAndPersistAccountFor("c4");

        // execute
        List<Account> entitiesFromDb = dao.getEntities(Arrays.asList(acc1.getId(), acc3.getId()));

        // verify
        assertEquals(2, entitiesFromDb.size());
        assertEqualAccounts(acc1, entitiesFromDb.get(0));
        assertEqualAccounts(acc3, entitiesFromDb.get(1));
    }

    private Account createAndPersistAccountFor(final String clientName) {

        Client client = createAndPersistClient(clientName);
        Account account = AccountTestFactory.createAccount(client);
        dao.storeEntity(account);
        return account;
    }

    private Client createAndPersistClient(String name) {

        Client client = ClientTestFactory.createClient(name);
        ClientDaoImpl.getInstance().storeEntity(client);
        return client;
    }

    @Test
    public void givenMultipleAccountsWhenGetAllThenReturnCorrectValues() {

        // setup
        Account acc1 = createAndPersistAccountFor("c1");
        Account acc2 = createAndPersistAccountFor("c2");
        Account acc3 = createAndPersistAccountFor("c3");

        // execute
        List<Account> entitiesFromDb = dao.getAllEntities();

        // verify
        assertEquals(3, entitiesFromDb.size());
        assertEqualAccounts(acc1, entitiesFromDb.get(0));
        assertEqualAccounts(acc2, entitiesFromDb.get(1));
        assertEqualAccounts(acc3, entitiesFromDb.get(2));
    }

    @Test
    public void givenAccountWhenDeleteThenSuccess() {

        // setup
        Account c1 = createAndPersistAccountFor("C1");

        // execute
        Long id = c1.getId();
        dao.delete(id);

        // verify
        Account entityFromDb = dao.getEntity(id);
        assertNull(entityFromDb);
    }

    @Test
    public void givenMissingAccountWhenDeleteThenException() {

        // setup
        thrown.expect(AccountNotFoundException.class);

        // execute
        dao.delete(1L);
    }
}