package com.monese.impl;

import com.monese.dao.EntityDao;
import com.monese.dao.exception.TransactionNotFoundException;
import com.monese.dao.impl.AccountDaoImpl;
import com.monese.dao.impl.TransactionDaoImpl;
import com.monese.model.Account;
import com.monese.model.Transaction;
import com.monese.testFactory.AccountTestFactory;
import com.monese.testFactory.TransactionTestFactory;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Test class for {@link TransactionDaoImpl}
 *
 * @author acozma
 */
public class TransactionDaoImplTest extends AbstractEntityDaoImplTest<Transaction> {

    @Override
    protected EntityDao<Transaction> createDao() {

        return TransactionDaoImpl.getInstance();
    }

    @Override
    protected Transaction createEntity() {

        return TransactionTestFactory.createTransaction(null,null,3L,4L);
    }

    @Test
    public void givenNewTransactionWhenStoreThenTransactionStored() {

        // setup
        Transaction transaction = createEntity();

        // execute
        dao.storeEntity(transaction);

        // verify
        Transaction entityFromDb = dao.getEntity(transaction.getId());
        assertEqualTransactions(transaction, entityFromDb);
    }

    private void assertEqualTransactions(final Transaction transaction, final Transaction entityFromDb) {

        assertEquals(transaction.getId(), entityFromDb.getId());
        assertEquals(transaction.getAmount(), entityFromDb.getAmount());
        assertEquals(transaction.getChildTransactionLinkId(), entityFromDb.getChildTransactionLinkId());
        assertEquals(transaction.getCreationDate(), entityFromDb.getCreationDate());
        assertEquals(transaction.getType(), entityFromDb.getType());
        assertEqualParentAccount(transaction, entityFromDb);
    }

    private void assertEqualParentAccount(final Transaction transaction, final Transaction entityFromDb) {

        Account parentAccount = transaction.getParentAccount();
        if (parentAccount != null) {
            assertEquals(parentAccount.getId(), entityFromDb.getParentAccount().getId());
        }
    }

    @Test
    public void givenExistingTransactionWhenUpdateThenCheckNewField() {

        // setup
        Transaction transaction = createEntity();
        dao.storeEntity(transaction);

        Account account = createAndPersistAccount();
        transaction.setParentAccount(account);

        // execute
        dao.updateEntity(transaction);

        // verify
        Transaction entityFromDb = dao.getEntity(transaction.getId());

        assertEqualTransactions(transaction, entityFromDb);

    }

    private Account createAndPersistAccount() {

        Account account = AccountTestFactory.createAccount();
        AccountDaoImpl.getInstance().storeEntity(account);
        return account;
    }

    @Test
    public void givenMultipleTransactionsWhenGetByIdsThenReturnCorrectValues() {

        // setup
        Transaction t1 = createEntity();
        dao.storeEntity(t1);
        Transaction t2 = createEntity();
        dao.storeEntity(t2);
        Transaction t3 = createEntity();
        dao.storeEntity(t3);

        // execute
        List<Transaction> entitiesFromDb = dao.getEntities(Arrays.asList(t1.getId(), t3.getId()));

        // verify
        assertEquals(2, entitiesFromDb.size());
        assertEqualTransactions(t1, entitiesFromDb.get(0));
        assertEqualTransactions(t3, entitiesFromDb.get(1));
    }

    @Test
    public void givenMultipleTransactionsWhenGetAllThenReturnCorrectValues() {

        // setup
        Transaction t1 = createEntity();
        dao.storeEntity(t1);
        Transaction t2 = createEntity();
        dao.storeEntity(t2);
        Transaction t3 = createEntity();
        dao.storeEntity(t3);

        // execute
        List<Transaction> entitiesFromDb = dao.getAllEntities();

        // verify
        assertEquals(3, entitiesFromDb.size());
        assertEqualTransactions(t1, entitiesFromDb.get(0));
        assertEqualTransactions(t2, entitiesFromDb.get(1));
        assertEqualTransactions(t3, entitiesFromDb.get(2));
    }

    @Test
    public void givenTransactionWhenDeleteThenSuccess() {

        // setup
        Transaction transaction = createEntity();
        dao.storeEntity(transaction);

        // execute
        Long id = transaction.getId();
        dao.delete(id);

        // verify
        Transaction entityFromDb = dao.getEntity(id);
        assertNull(entityFromDb);
    }

    @Test
    public void givenMissingAccountWhenDeleteThenException() {

        // setup
        thrown.expect(TransactionNotFoundException.class);

        // execute
        dao.delete(1L);
    }
}