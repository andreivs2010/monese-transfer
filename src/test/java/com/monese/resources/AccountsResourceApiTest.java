package com.monese.resources;

import com.monese.EntityManagerUtils;
import com.monese.api.dto.TransferDetailsDto;
import com.monese.api.resources.AccountsResource;
import com.monese.dao.impl.AccountDaoImpl;
import com.monese.dao.impl.ClientDaoImpl;
import com.monese.model.Account;
import com.monese.model.Client;
import com.monese.testFactory.AccountTestFactory;
import com.monese.testFactory.ClientTestFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Test class for {@link AccountsResource}
 *
 * @author acozma
 */
public class AccountsResourceApiTest extends JerseyTest {

	@Override
	protected Application configure() {

		return new ResourceConfig(AccountsResource.class);
	}

	@Override
	@After
	public void tearDown() throws Exception {

		super.tearDown();
		List<Account> allEntities = AccountDaoImpl.getInstance().getAllEntities();
		allEntities.stream().forEach(account -> AccountDaoImpl.getInstance().delete(account.getId()));
		EntityManagerUtils.closeEntityManagerFactory();
	}

	@Test
	public void givenExistingAccountsWhenTransferWithNullDetailsThenCheckResponse() {

		// setup
		Client sherlock = ClientTestFactory.createClient("Sherlock Holmes");
		ClientDaoImpl.getInstance().storeEntity(sherlock);
		Client watson = ClientTestFactory.createClient("John Watson");
		ClientDaoImpl.getInstance().storeEntity(watson);

		Account sourceAccount = AccountTestFactory.createAccount(sherlock);
		AccountDaoImpl.getInstance().storeEntity(sourceAccount);

		Account targetAccount = AccountTestFactory.createAccount(watson);
		AccountDaoImpl.getInstance().storeEntity(targetAccount);

		Long sourceAccountId = sourceAccount.getId();
		// execute

		Entity<TransferDetailsDto> transferDetails = Entity.entity(null, MediaType.APPLICATION_JSON);

		Response response = target("/accounts/"+sourceAccountId+":transfer")
				.request()
				.post(transferDetails);


		// verify
		assertEquals(response.getStatus(), 400);
		assertEquals(response.readEntity(String.class), "Transfer details object cannot be null");
	}

	@Test
	public void givenInvalidAmountWhenMakingTransferThenCheckResponse() {

		// setup
		Client sherlock = ClientTestFactory.createClient("Sherlock Holmes");
		ClientDaoImpl.getInstance().storeEntity(sherlock);
		Client watson = ClientTestFactory.createClient("John Watson");
		ClientDaoImpl.getInstance().storeEntity(watson);

		Account sourceAccount = AccountTestFactory.createAccount(sherlock);
		AccountDaoImpl.getInstance().storeEntity(sourceAccount);

		Account targetAccount = AccountTestFactory.createAccount(watson);
		AccountDaoImpl.getInstance().storeEntity(targetAccount);

		Long sourceAccountId = sourceAccount.getId();

		TransferDetailsDto transferDetailsDto = new TransferDetailsDto();
		transferDetailsDto.setTargetAccountId(targetAccount.getAccountId());
		transferDetailsDto.setAmount(BigDecimal.valueOf(-10));
		Entity<TransferDetailsDto> transferDetails = Entity.entity(transferDetailsDto, MediaType.APPLICATION_JSON);

		Response response = target("/accounts/" + sourceAccountId + ":transfer")
				.request()
				.post(transferDetails);


		// verify
		assertEquals(response.getStatus(), 400);
		assertEquals(response.readEntity(String.class), "Amount is not valid");

	}

	@Test
	public void givenSameAccountAsTargetWhenMakingTransferThenCheckResponse() {

		// setup
		Client sherlock = ClientTestFactory.createClient("Sherlock Holmes");
		ClientDaoImpl.getInstance().storeEntity(sherlock);
		Client watson = ClientTestFactory.createClient("John Watson");
		ClientDaoImpl.getInstance().storeEntity(watson);

		Account sourceAccount = AccountTestFactory.createAccount(sherlock);
		AccountDaoImpl.getInstance().storeEntity(sourceAccount);

		Long sourceAccountId = sourceAccount.getId();

		TransferDetailsDto transferDetailsDto = new TransferDetailsDto();
		transferDetailsDto.setTargetAccountId(sourceAccount.getAccountId());
		transferDetailsDto.setAmount(BigDecimal.ONE);
		Entity<TransferDetailsDto> transferDetails = Entity.entity(transferDetailsDto, MediaType.APPLICATION_JSON);

		Response response = target("/accounts/" + sourceAccountId + ":transfer")
				.request()
				.post(transferDetails);


		// verify
		assertEquals(response.getStatus(), 400);
		assertEquals(response.readEntity(String.class), "Cannot transfer in the same account");

	}

	@Test
	public void givenInsuficientFundsWhenMakingTransferThenCheckResponse() {

		// setup
		Client sherlock = ClientTestFactory.createClient("Sherlock Holmes");
		ClientDaoImpl.getInstance().storeEntity(sherlock);
		Client watson = ClientTestFactory.createClient("John Watson");
		ClientDaoImpl.getInstance().storeEntity(watson);

		Account sourceAccount = AccountTestFactory.createAccount(sherlock);
		AccountDaoImpl.getInstance().storeEntity(sourceAccount);

		Long sourceAccountId = sourceAccount.getId();

		Account targetAccount = AccountTestFactory.createAccount(watson);
		AccountDaoImpl.getInstance().storeEntity(targetAccount);

		TransferDetailsDto transferDetailsDto = new TransferDetailsDto();
		transferDetailsDto.setTargetAccountId(targetAccount.getAccountId());
		transferDetailsDto.setAmount(BigDecimal.valueOf(200));
		Entity<TransferDetailsDto> transferDetails = Entity.entity(transferDetailsDto, MediaType.APPLICATION_JSON);

		Response response = target("/accounts/" + sourceAccountId + ":transfer")
				.request()
				.post(transferDetails);


		// verify
		assertEquals(response.getStatus(), 400);
		assertEquals(response.readEntity(String.class), "Transfer declined. Insufficient funds");

	}

	@Test
	public void givenMissingSourceAccountWhenMakingTransferThenCheckResponse() {

		// setup
		Client watson = ClientTestFactory.createClient("John Watson");
		ClientDaoImpl.getInstance().storeEntity(watson);

		Account targetAccount = AccountTestFactory.createAccount(watson);
		AccountDaoImpl.getInstance().storeEntity(targetAccount);

		TransferDetailsDto transferDetailsDto = new TransferDetailsDto();
		transferDetailsDto.setTargetAccountId(targetAccount.getAccountId());
		transferDetailsDto.setAmount(BigDecimal.valueOf(15));
		Entity<TransferDetailsDto> transferDetails = Entity.entity(transferDetailsDto, MediaType.APPLICATION_JSON);

		Long sourceAccountId = 10L;
		Response response = target("/accounts/" + sourceAccountId + ":transfer")
				.request()
				.post(transferDetails);


		// verify
		assertEquals(response.getStatus(), 400);
		assertEquals(response.readEntity(String.class), "Missing source account");

	}

	@Test
	public void givenMissingTargetAccountWhenMakingTransferThenCheckResponse() {

		// setup
		Client sherlock = ClientTestFactory.createClient("Sherlock Holmes");
		ClientDaoImpl.getInstance().storeEntity(sherlock);


		Account sourceAccount = AccountTestFactory.createAccount(sherlock);
		AccountDaoImpl.getInstance().storeEntity(sourceAccount);

		Long sourceAccountId = sourceAccount.getId();

		TransferDetailsDto transferDetailsDto = new TransferDetailsDto();
		Long targetAccountId=10L;
		transferDetailsDto.setTargetAccountId(targetAccountId);
		transferDetailsDto.setAmount(BigDecimal.valueOf(15));
		Entity<TransferDetailsDto> transferDetails = Entity.entity(transferDetailsDto, MediaType.APPLICATION_JSON);

		Response response = target("/accounts/" + sourceAccountId + ":transfer")
				.request()
				.post(transferDetails);


		// verify
		assertEquals(response.getStatus(), 400);
		assertEquals(response.readEntity(String.class), "Missing target account");

	}

	@Test
	public void givenExistingAccountsWhenTransferThenCheckCorrectResult() {

		// setup
		Client sherlock = ClientTestFactory.createClient("Sherlock Holmes");
		ClientDaoImpl.getInstance().storeEntity(sherlock);
		Client watson = ClientTestFactory.createClient("John Watson");
		ClientDaoImpl.getInstance().storeEntity(watson);

		Account sourceAccount = AccountTestFactory.createAccount(sherlock);
		AccountDaoImpl.getInstance().storeEntity(sourceAccount);

		Account targetAccount = AccountTestFactory.createAccount(watson);
		AccountDaoImpl.getInstance().storeEntity(targetAccount);

		Long sourceAccountId = sourceAccount.getId();
		// execute

		TransferDetailsDto transferDetailsDto = new TransferDetailsDto();
		transferDetailsDto.setTargetAccountId(targetAccount.getAccountId());
		transferDetailsDto.setAmount(BigDecimal.TEN);
		Entity<TransferDetailsDto> transferDetails = Entity.entity(transferDetailsDto, MediaType.APPLICATION_JSON);

		Response response = target("/accounts/"+sourceAccountId+":transfer")
				.request()
				.post(transferDetails);


		// verify
		assertEquals(response.getStatus(), 200);
	}
}