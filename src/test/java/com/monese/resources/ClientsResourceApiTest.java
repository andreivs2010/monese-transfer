package com.monese.resources;

import com.monese.EntityManagerUtils;
import com.monese.api.resources.ClientsResource;
import com.monese.dao.impl.ClientDaoImpl;
import com.monese.model.Client;
import com.monese.testFactory.ClientTestFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Application;

import static org.junit.Assert.assertEquals;

public class ClientsResourceApiTest extends JerseyTest {

	private Client sherlock = ClientTestFactory.createClient("Sherlock Holmes");
	private Client watson = ClientTestFactory.createClient("John Watson");
	@Override
	protected Application configure() {

		return new ResourceConfig(ClientsResource.class);
	}

	@Override
	@Before
	public void setUp() throws Exception {

		super.setUp();
		ClientDaoImpl.getInstance().storeEntity(sherlock);
		ClientDaoImpl.getInstance().storeEntity(watson);

	}

	@Override
	@After
	public void tearDown() throws Exception {

		super.tearDown();
		ClientDaoImpl.getInstance().delete(1L);
		ClientDaoImpl.getInstance().delete(2L);
		EntityManagerUtils.closeEntityManagerFactory();
	}

	@Test
	public void givenExistingClientWhenGetThenReturnAllClient() {

		// setup

		// execute
		final String response = target("/clients")
				.request()
				.get(String.class);

		// verify
		String expectedResponse = "[{\"clientId\":1,\"name\":\"Sherlock Holmes\",\"state\":\"ACTIVE\",\"numericPersonalCode\":\"19272183126\",\"phoneNumber\":\"0747356347\",\"address\":\"221B Backer Street\",\"creationDate\":\"2019-11-03T15:30:45\",\"lastModifiedDate\":\"2019-11-03T15:30:45\",\"accountIds\":[]},{\"clientId\":2,\"name\":\"John Watson\",\"state\":\"ACTIVE\",\"numericPersonalCode\":\"19272183126\",\"phoneNumber\":\"0747356347\",\"address\":\"221B Backer Street\",\"creationDate\":\"2019-11-03T15:30:45\",\"lastModifiedDate\":\"2019-11-03T15:30:45\",\"accountIds\":[]}]";
		assertEquals(expectedResponse, response);
	}

	@Test
	public void givenExistingClientWhenGetByIdThenReturnClient() {

		// setup
		Client sherlock = ClientTestFactory.createClient("Sherlock Holmes");
		ClientDaoImpl.getInstance().storeEntity(sherlock);
		Client watson = ClientTestFactory.createClient("John Watson");
		ClientDaoImpl.getInstance().storeEntity(watson);

		// execute
		final String response = target("/clients/2")
				.request()
				.get(String.class);

		// verify
		String expectedResponse = "{\"clientId\":2,\"name\":\"John Watson\",\"state\":\"ACTIVE\",\"numericPersonalCode\":\"19272183126\",\"phoneNumber\":\"0747356347\",\"address\":\"221B Backer Street\",\"creationDate\":\"2019-11-03T15:30:45\",\"lastModifiedDate\":\"2019-11-03T15:30:45\",\"accountIds\":[]}";
		assertEquals(expectedResponse, response);
	}
}