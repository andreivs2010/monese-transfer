package com.monese.resources;

import com.monese.api.dto.AccountDto;
import com.monese.api.dto.ClientDto;
import com.monese.api.resources.ClientsResource;
import com.monese.service.ClientService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.OK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ClientsResourceMockTest {

	@Mock
	private ClientService clientServiceMock;

	@InjectMocks
	private ClientsResource resource;

	@Before
	public void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void givenExistingClientsWhenGetThenReturnAllClients() {

		// setup
		List<ClientDto> dtos = new ArrayList<>();

		when(clientServiceMock.getAllClients()).thenReturn(dtos);

		// execute
		Response response = resource.get();

		// verify
		assertEquals(OK.getStatusCode(), response.getStatus());
		assertSame(dtos, response.getEntity());
	}

	@Test
	public void givenExistingClientWhenGetByIdThenReturnCorrectClient() {

		// setup
		ClientDto clientDto = new ClientDto();
		when(clientServiceMock.getClient(eq(1L))).thenReturn(clientDto);

		// execute
		Response response = resource.getById(1L);

		// verify
		assertEquals(OK.getStatusCode(), response.getStatus());
		assertSame(clientDto, response.getEntity());

	}

	@Test
	public void givenNewClientDtoWhenCreateThenReturnCreatedClient() {

		// setup
		ClientDto input = new ClientDto();
		ClientDto expectedResult = new ClientDto();
		when(clientServiceMock.storeClient(eq(input))).thenReturn(expectedResult);

		// execute
		Response response = resource.create(input);

		// verify
		assertEquals(CREATED.getStatusCode(), response.getStatus());
		assertSame(expectedResult, response.getEntity());
	}

	@Test
	public void givenExistingClientDtoWhenUpdateThenReturnUpdatedClient() {

		// setup
		ClientDto input = new ClientDto();
		ClientDto expectedResult = new ClientDto();
		when(clientServiceMock.updateClient(eq(1L), eq(input))).thenReturn(expectedResult);

		// execute
		Response response = resource.update(1L, input);

		// verify
		assertEquals(OK.getStatusCode(), response.getStatus());
		assertSame(expectedResult, response.getEntity());
	}

	@Test
	public void whenDeleteThenUseService() {

		// execute
		Response response = resource.delete(1L);

		// verify
		assertEquals(OK.getStatusCode(), response.getStatus());
		verify(clientServiceMock).deleteClient(eq(1L));
	}

	@Test
	public void whenGetAccountsForClientByIdThenUserService() {

		// setup
		List<AccountDto> accounts = new ArrayList<>();
		when(clientServiceMock.getAccountsOfClientBy(eq(1L))).thenReturn(accounts);

		// execute
		Response response = resource.getAccountsOfClientBy(1L);

		// verify
		assertEquals(OK.getStatusCode(), response.getStatus());
		assertSame(accounts, response.getEntity());
	}
}