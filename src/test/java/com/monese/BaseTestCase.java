package com.monese;

import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 * Base class for all tests
 *
 * @author acozma
 */
public class BaseTestCase {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    /**
     * Asserts that an exception of a given type will be thrown
     *
     * @param exceptionClass
     * 		the class of the expected exception. Never {@code null}
     */
    protected <T extends Throwable> void expectExceptionClass(Class<T> exceptionClass) {

        thrown.expect(exceptionClass);

    }

    /**
     * Asserts that a trowed exception matches given input
     *
     * @param exceptionMatcher
     * 		matcher input. Never {@code null}
     */
    protected <T extends TypeSafeMatcher> void expectExceptionMatcher(T exceptionMatcher) {

        thrown.expect(exceptionMatcher);

    }

}
