package com.monese.model;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class AccountTest {

	@Test
	public void givenAccountBalanceWhenAddAmountThenCheckCorrectValue() {
		//setup
		Account account = new Account();
		account.setBalance(BigDecimal.TEN);

		//execute
		account.addAmount(BigDecimal.TEN);

		//verify
		assertEquals(BigDecimal.valueOf(20),account.getBalance());
	}

	@Test
	public void givenAccountBalanceWhenSubtractAmountThenCheckCorrectValue() {

		//setup
		Account account = new Account();
		account.setBalance(BigDecimal.TEN);

		//execute
		account.subtractAmount(BigDecimal.TEN);

		//verify
		assertEquals(BigDecimal.ZERO,account.getBalance());
	}
}