package com.monese.service;

import com.monese.BaseTestCase;
import com.monese.api.dto.TransferDetailsDto;
import com.monese.dao.impl.AccountDaoImpl;
import com.monese.dao.impl.TransactionDaoImpl;
import com.monese.model.Account;
import com.monese.model.Transaction;
import com.monese.service.exception.AmountValidationException;
import com.monese.service.exception.TransferException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AccountDaoImpl.class, TransactionDaoImpl.class})
public class TransferServiceMocksTest extends BaseTestCase {

	private TransferService service ;

	private static AccountDaoImpl accountDaoMock;
	private static TransactionDaoImpl transactionDaoMock;

	@BeforeClass
	public static void beforeClass() {

		mockStatic(AccountDaoImpl.class);
		mockStatic(TransactionDaoImpl.class);
		accountDaoMock = mock(AccountDaoImpl.class);
		transactionDaoMock = mock(TransactionDaoImpl.class);
		PowerMockito.when(AccountDaoImpl.getInstance()).thenReturn(accountDaoMock);
		PowerMockito.when(TransactionDaoImpl.getInstance()).thenReturn(transactionDaoMock);
	}

	@Mock
	private Account sourceAccountMock;

	@Before
	public void setUp() {

		service= TransferService.getInstance();
		when(accountDaoMock.getEntity(1L)).thenReturn(sourceAccountMock);
	}

	@Test
	public void givenNegativeAmountWhenMakingTransferThenThrowException() throws Exception{

		//setup
		TransferDetailsDto transferDetails = new TransferDetailsDto();
		transferDetails.setAmount(BigDecimal.valueOf(-20));
		expectExceptionClass(AmountValidationException.class);

		//execute
		try {
			service.transfer(sourceAccountMock.getAccountId(), transferDetails);
		} catch (AmountValidationException | TransferException e) {

			//verify
			assertTrue(e instanceof AmountValidationException);
			assertEquals(AmountValidationException.Reason.INCORRECT_AMOUNT,((AmountValidationException)e).getReason());
			throw e;

		}
	}

	@Test
	public void givenMissingSourceAccountWhenMakingTransferThenExpectException() throws Exception {

		//setup
		when(accountDaoMock.getEntity(2L)).thenReturn(null);

		TransferDetailsDto transferDetails = new TransferDetailsDto();
		transferDetails.setTargetAccountId(3L);
		transferDetails.setAmount(BigDecimal.valueOf(20));
		expectExceptionClass(TransferException.class);

		//execute
		try {
			service.transfer(2L, transferDetails);
		} catch (AmountValidationException | TransferException e) {

			//verify
			assertTrue(e instanceof TransferException);
			assertEquals(TransferException.Reason.MISSING_SOURCE_ACCOUNT,((TransferException)e).getReason());
			throw e;

		}
	}

	@Test
	public void givenMissingTargetAccountWhenMakingTransferThenExpectException() throws Exception {

		//setup
		when(accountDaoMock.getEntity(2L)).thenReturn(null);

		TransferDetailsDto transferDetails = new TransferDetailsDto();
		transferDetails.setTargetAccountId(2L);
		transferDetails.setAmount(BigDecimal.valueOf(20));
		expectExceptionClass(TransferException.class);

		//execute
		try {
			service.transfer(1L, transferDetails);
		} catch (AmountValidationException | TransferException e) {

			//verify
			assertTrue(e instanceof TransferException);
			assertEquals(TransferException.Reason.MISSING_TARGET_ACCOUNT,((TransferException)e).getReason());
			throw e;

		}
	}

	@Test
	public void givenSameAccountAsSourceAndTargetWhenMakingTransferThenExpectException() throws Exception {

		//setup

		TransferDetailsDto transferDetails = new TransferDetailsDto();
		transferDetails.setTargetAccountId(1L);
		transferDetails.setAmount(BigDecimal.valueOf(20));
		expectExceptionClass(TransferException.class);

		//execute
		try {
			service.transfer(1L, transferDetails);
		} catch (AmountValidationException | TransferException e) {

			//verify
			assertTrue(e instanceof TransferException);
			assertEquals(TransferException.Reason.SAME_ACCOUNT,((TransferException)e).getReason());
			throw e;

		}
	}

	@Test
	public void givenInsufficientBalanceWhenMakingTransferThenExpectException() throws Exception {

		//setup
		Account targetAccount = mock(Account.class);
		when(accountDaoMock.getEntity(2L)).thenReturn(targetAccount);
		when(sourceAccountMock.getBalance()).thenReturn(BigDecimal.TEN);
		TransferDetailsDto transferDetails = new TransferDetailsDto();
		transferDetails.setTargetAccountId(2L);
		transferDetails.setAmount(BigDecimal.valueOf(20));
		expectExceptionClass(TransferException.class);

		//execute
		try {
			service.transfer(1L, transferDetails);
		} catch (AmountValidationException | TransferException e) {

			//verify
			assertTrue(e instanceof TransferException);
			assertEquals(TransferException.Reason.INSUFICIENT_FUNDS,((TransferException)e).getReason());
			throw e;

		}
	}

	@Test
	public void givenSourceAndTargetAccountsWhenMakingTransferThenCheckInteractions() throws Exception{

		//setup
		Account targetAccountMock = mock(Account.class);
		when(accountDaoMock.getEntity(2L)).thenReturn(targetAccountMock);
		when(sourceAccountMock.getBalance()).thenReturn(BigDecimal.valueOf(100));
		TransferDetailsDto transferDetails = new TransferDetailsDto();
		transferDetails.setTargetAccountId(2L);
		transferDetails.setAmount(BigDecimal.valueOf(20));

		service.transfer(1L, transferDetails);

		//TODO - can refactor code in a way to do not check with any; possible solution can be to
		//extract in TransferService TransactionCreator which creates the new transactions
		Mockito.verify(transactionDaoMock,Mockito.times(2)).storeAndTouchEntity(Mockito.any(Transaction.class));
		Mockito.verify(transactionDaoMock).updateAndTouchEntity(Mockito.any(Transaction.class));
		Mockito.verify(transactionDaoMock).updateAndTouchEntity(Mockito.any(Transaction.class));
		Mockito.verify(accountDaoMock,Mockito.times(2)).updateAndTouchEntity(Mockito.any(Account.class));
		Mockito.verify(sourceAccountMock).subtractAmount(BigDecimal.valueOf(20));
		Mockito.verify(targetAccountMock).addAmount(BigDecimal.valueOf(20));
	}
}