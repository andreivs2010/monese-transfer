package com.monese.service;

import com.monese.api.dto.TransactionDto;
import com.monese.dao.impl.TransactionDaoImpl;
import com.monese.mapper.TransactionMapperEnhancer;
import com.monese.model.Transaction;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertSame;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ TransactionDaoImpl.class, TransactionMapperEnhancer.class })
public class TransactionServiceTest {

	private static TransactionDaoImpl daoMock;
	private static TransactionMapperEnhancer mapperMock;

	private TransactionService service;

	@BeforeClass
	public static void beforeClass() {

		mockStatic(TransactionDaoImpl.class);
		daoMock = PowerMockito.mock(TransactionDaoImpl.class);
		PowerMockito.when(TransactionDaoImpl.getInstance()).thenReturn(daoMock);

		mockStatic(TransactionMapperEnhancer.class);
		mapperMock = PowerMockito.mock(TransactionMapperEnhancer.class);
		PowerMockito.when(TransactionMapperEnhancer.getInstance()).thenReturn(mapperMock);

	}

	@Before
	public void setUp() {

		service = TransactionService.getInstance();

	}

	@Test
	public void givenTransactionDtoWhenStoreThenCheckInteractions() {

		// setup
		final TransactionDto input = new TransactionDto();
		final Transaction entityMock = mock(Transaction.class);
		final TransactionDto createdDto = mock(TransactionDto.class);

		when(mapperMock.mapDtoToEntity(input)).thenReturn(entityMock);
		when(mapperMock.mapEntityToDto(eq(entityMock))).thenReturn(createdDto);

		// execute
		TransactionDto dto = service.create(input);

		// verify
		verify(daoMock).storeAndTouchEntity(eq(entityMock));
		assertSame(createdDto, dto);

	}
}