package com.monese.service;

import com.monese.api.dto.ClientDto;
import com.monese.dao.impl.ClientDaoImpl;
import com.monese.mapper.ClientMapper;
import com.monese.model.Client;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ClientDaoImpl.class, ClientMapper.class })
public class ClientServiceTest {

	private ClientService service;

	private static ClientDaoImpl clientDaoMock;
	private static ClientMapper clientMapperMock;

	@BeforeClass
	public static void beforeClass() {

		mockStatic(ClientDaoImpl.class);
		clientDaoMock = PowerMockito.mock(ClientDaoImpl.class);
		PowerMockito.when(ClientDaoImpl.getInstance()).thenReturn(clientDaoMock);

		mockStatic(ClientMapper.class);
		clientMapperMock = PowerMockito.mock(ClientMapper.class);
		PowerMockito.when(ClientMapper.getInstance()).thenReturn(clientMapperMock);

	}

	@Before
	public void setUp() {

		service = ClientService.getInstance();

	}

	@Test
	public void givenClientDtoWhenStoreClientThenCheckInteractionsWithDao() {

		// setup
		ClientDto clientDto = new ClientDto();
		Client clientMock = mock(Client.class);

		when(clientMapperMock.mapDtoToEntity(eq(clientDto))).thenReturn(clientMock);
		when(clientMapperMock.mapEntityToDto(eq(clientMock))).thenReturn(clientDto);

		// execute
		ClientDto resultDto = service.storeClient(clientDto);

		// verify
		verify(clientDaoMock).storeAndTouchEntity(eq(clientMock));
		assertEquals(clientDto, resultDto);
	}

	@Test
	public void updateClient() {
		//TODO add test for updating client
	}

	@Test
	public void getClient() {
		//TODO add test for getting client
	}

	@Test
	public void getAllClients() {
		//TODO add test for getting all clients
	}
}