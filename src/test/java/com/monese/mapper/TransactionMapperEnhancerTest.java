package com.monese.mapper;

import com.monese.api.dto.TransactionDto;
import com.monese.dao.impl.AccountDaoImpl;
import com.monese.dao.impl.ClientDaoImpl;
import com.monese.model.Account;
import com.monese.model.Client;
import com.monese.model.Transaction;
import com.monese.testFactory.AccountTestFactory;
import com.monese.testFactory.ClientTestFactory;
import com.monese.testFactory.TransactionTestFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.monese.testFactory.TransactionDtoTestFactory.createTransactionDto;
import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;
import static java.util.stream.IntStream.range;
import static org.junit.Assert.assertEquals;

public class TransactionMapperEnhancerTest {

	private TransactionMapperEnhancer mapper;

	@Before
	public void setUp() {

		mapper = TransactionMapperEnhancer.getInstance();
	}

	@Test
	public void givenDtoWithFieldsWhenMapToEntityThenCopyAllFields() {

		// setup
		TransactionDto dto = createTransactionDto();

		// execute
		Transaction entity = mapper.mapDtoToEntity(dto);

		// verify
		assertDtoMappedCorrectlyToEntity(dto, entity);

	}

	private void assertDtoMappedCorrectlyToEntity(final TransactionDto dto, final Transaction entity) {

		assertEquals(dto.getAmount(), entity.getAmount());
		assertEquals(dto.getChildTransactionLinkId(), entity.getChildTransactionLinkId());
		assertEquals(dto.getParentTransactionLinkId(), entity.getParentTransactionLinkId());
		assertEquals(dto.getTransactionId(), entity.getTransactionId());
		assertEquals(dto.getType().name(), entity.getType().name());
		assertEquals(dto.getParentAccountId(), entity.getParentAccount().getId());
		assertEquals(dto.getCreationDate(), ISO_DATE_TIME.format(entity.getCreationDate()));
	}

	@Test
	public void givenEntityWhenMapToDtoThenCopyOnlyCertainFields() {

		// setup
		Transaction entity = createTransactionWith(1L, 2L, 3L, 4L);

		// execute
		TransactionDto dto = mapper.mapEntityToDto(entity);

		// verify
		assertEntityMappedCorrectlyToDto(entity, dto);
	}

	private Transaction createTransactionWith(final long transactionId, final long accountId,
											  final long parentTransactionLinkId, final long childTransactionLinkId) {

		Account account = new Account();
		account.setAccountId(accountId);
		return TransactionTestFactory.createTransaction(transactionId, account, parentTransactionLinkId,
				childTransactionLinkId);
	}

	private void assertEntityMappedCorrectlyToDto(final Transaction entity, final TransactionDto dto) {

		assertEquals(entity.getAmount(), dto.getAmount());
		assertEquals(entity.getChildTransactionLinkId(), dto.getChildTransactionLinkId());
		assertEquals(entity.getParentTransactionLinkId(), dto.getParentTransactionLinkId());
		assertEquals(entity.getTransactionId(), dto.getTransactionId());
		assertEquals(entity.getType().name(),dto.getType().name());
		assertEquals(entity.getParentAccount().getId(), dto.getParentAccountId());
		assertEquals(entity.getCreationDate(), LocalDateTime.parse(dto.getCreationDate(), ISO_DATE_TIME));
	}

	@Test
	public void givenDtosWhenMapToEntitiesThenCopyOnlySpecificValues() {

		// setup
		Account acc1 = createAndPersistAccountWith("Sherlock Holmes");
		Account acc2 = createAndPersistAccountWith("John Watson");

		TransactionDto dto1 = createTransactionDto(1L, acc1.getId(), 3L, 4L);
		TransactionDto dto2 = createTransactionDto(11L, acc2.getId(), 33L, 44L);

		List<TransactionDto> dtos = new ArrayList<>();
		dtos.add(dto1);
		dtos.add(dto2);

		// execute
		List<Transaction> entities = mapper.mapDtoListToEntityList(dtos);

		// verify
		range(0, dtos.size())
				.forEach(i -> assertDtoMappedCorrectlyToEntity(dtos.get(i), entities.get(i)));
	}

	private Account createAndPersistAccountWith(final String clientName) {

		Client client = ClientTestFactory.createClient(clientName);
		ClientDaoImpl.getInstance().storeEntity(client);

		Account account = AccountTestFactory.createAccount(client);
		AccountDaoImpl.getInstance().storeEntity(account);
		return account;
	}

	@Test
	public void givenEntitiesWhenMapToDtosThenCopyOnlyCertainFields() {

		// setup
		Transaction entity1 = createTransactionWith(1L, 2L, 3L, 4L);
		Transaction entity2 = createTransactionWith(11L, 22L, 33L, 44L);

		List<Transaction> entities = new ArrayList<>();
		entities.add(entity1);
		entities.add(entity2);

		// execute
		List<TransactionDto> dtos = mapper.mapEntityListToDtoList(entities);

		// verify
		range(0, dtos.size())
				.forEach(i -> assertEntityMappedCorrectlyToDto(entities.get(i), dtos.get(i)));
	}

}