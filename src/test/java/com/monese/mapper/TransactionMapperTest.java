package com.monese.mapper;

import com.monese.api.dto.TransactionDto;
import com.monese.model.Account;
import com.monese.model.Transaction;
import com.monese.testFactory.TransactionTestFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.monese.testFactory.TransactionDtoTestFactory.createTransactionDto;
import static java.util.stream.IntStream.range;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TransactionMapperTest {

	private TransactionMapper mapper;

	@Before
	public void setUp() {

		mapper = TransactionMapper.getInstance();
	}

	@Test
	public void givenDtoWithFieldsWhenMapToEntityThenCopyOnlyCertainFields() {

		// setup
		TransactionDto dto = createTransactionDto();

		// execute
		Transaction entity = mapper.mapDtoToEntity(dto);

		// verify
		assertDtoMappedCorrectlyToEntity(dto, entity);

	}

	private void assertDtoMappedCorrectlyToEntity(final TransactionDto dto, final Transaction entity) {

		assertEquals(dto.getAmount(), entity.getAmount());
		assertEquals(dto.getChildTransactionLinkId(), entity.getChildTransactionLinkId());
		assertEquals(dto.getParentTransactionLinkId(), entity.getParentTransactionLinkId());
		assertEquals(dto.getTransactionId(), entity.getTransactionId());
		assertNull(entity.getType());
		assertNull(entity.getParentAccount());
		assertNull(entity.getCreationDate());
	}

	@Test
	public void givenEntityWhenMapToDtoThenCopyOnlyCertainFields() {

		// setup
		Transaction entity = createTransactionWith(1L, 2L, 3L, 4L);

		// execute
		TransactionDto dto = mapper.mapEntityToDto(entity);

		// verify
		assertEntityMappedCorrectlyToDto(entity, dto);
	}

	private Transaction createTransactionWith(final long transactionId, final long accountId,
											  final long parentTransactionLinkId, final long childTransactionLinkId) {

		Account account = new Account();
		account.setAccountId(accountId);
		return TransactionTestFactory.createTransaction(transactionId, account, parentTransactionLinkId,
				childTransactionLinkId);
	}

	private void assertEntityMappedCorrectlyToDto(final Transaction entity, final TransactionDto dto) {

		assertEquals(entity.getAmount(), dto.getAmount());
		assertEquals(entity.getChildTransactionLinkId(), dto.getChildTransactionLinkId());
		assertEquals(entity.getParentTransactionLinkId(), dto.getParentTransactionLinkId());
		assertEquals(entity.getTransactionId(), dto.getTransactionId());
		assertNull(dto.getType());
		assertNull(dto.getParentAccountId());
		assertNull(dto.getCreationDate());
	}

	@Test
	public void givenDtosWhenMapToEntitiesThenCopyOnlySpecificValues() {

		// setup
		TransactionDto dto1 = createTransactionDto(1L, 2L, 3L, 4L);
		TransactionDto dto2 = createTransactionDto(11L, 22L, 33L, 44L);

		List<TransactionDto> dtos = new ArrayList<>();
		dtos.add(dto1);
		dtos.add(dto2);

		// execute
		List<Transaction> entities = mapper.mapDtoListToEntityList(dtos);

		// verify
		range(0, dtos.size())
				.forEach(i -> assertDtoMappedCorrectlyToEntity(dtos.get(i), entities.get(i)));
	}

	@Test
	public void givenEntitiesWhenMapToDtosThenCopyOnlyCertainFields() {

		// setup
		Transaction entity1 = createTransactionWith(1L, 2L, 3L, 4L);
		Transaction entity2 = createTransactionWith(11L, 22L, 33L, 44L);

		List<Transaction> entities = new ArrayList<>();
		entities.add(entity1);
		entities.add(entity2);

		// execute
		List<TransactionDto> dtos = mapper.mapEntityListToDtoList(entities);

		// verify
		range(0, dtos.size())
				.forEach(i -> assertEntityMappedCorrectlyToDto(entities.get(i), dtos.get(i)));
	}
}