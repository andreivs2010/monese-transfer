package com.monese.mapper;

import com.monese.api.dto.ClientDto;
import com.monese.model.Account;
import com.monese.model.Client;
import com.monese.testFactory.ClientDtoTestFactory;
import com.monese.testFactory.ClientTestFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;
import static org.junit.Assert.assertEquals;

/**
 * Test class for {@link ClientMapper}
 *
 * @author acozma
 */
public class ClientMapperTest {

	private ClientMapper mapper;

	@Before
	public void setUp() {

		mapper = ClientMapper.getInstance();
	}

	@Test
	public void givenClientWhenMapToDtoThenCheckCorrectMapping() {

		// setup
		Client client = ClientTestFactory.createClient();

		// execute
		ClientDto clientDto = mapper.mapEntityToDto(client);

		// verify
		assertEqualValues(client, clientDto);
	}

	private void assertEqualValues(final Client client, final ClientDto clientDto) {

		assertEquals(client.getClientId(), clientDto.getClientId());
		assertEquals(client.getName(), clientDto.getName());
		assertEquals(client.getState().name(), clientDto.getState().name());
		assertEquals(client.getAddress(), clientDto.getAddress());
		assertEquals(client.getPhoneNumber(), clientDto.getPhoneNumber());
		assertEquals(client.getNumericPersonalCode(), clientDto.getNumericPersonalCode());
		assertEquals(ISO_DATE_TIME.format(client.getCreationDate()), clientDto.getCreationDate());
		assertEquals(ISO_DATE_TIME.format(client.getLastModifiedDate()), clientDto.getLastModifiedDate());

		List<Account> accounts = client.getAccounts();
		Set<Long> accountIds = accounts.stream().map(Account::getId).collect(Collectors.toSet());
		assertEquals(accountIds, new HashSet<>(clientDto.getAccountIds()));
	}

	@Test
	public void givenClientDtoWhenMapToEntityThenCheckCorrectMapping() {

		// setup
		ClientDto clientDto = ClientDtoTestFactory.createClientDto(1L);

		// execute
		Client client = mapper.mapDtoToEntity(clientDto);

		// verify
		assertEqualValues(clientDto, client);
	}

	private void assertEqualValues(final ClientDto clientDto, final Client client) {

		assertEquals(clientDto.getName(), client.getName());
		assertEquals(clientDto.getState().name(), client.getState().name());
		assertEquals(clientDto.getAddress(), client.getAddress());
		assertEquals(clientDto.getPhoneNumber(), client.getPhoneNumber());
		assertEquals(clientDto.getNumericPersonalCode(), client.getNumericPersonalCode());
		assertEquals(clientDto.getCreationDate(), ISO_DATE_TIME.format(client.getCreationDate()));
		assertEquals(clientDto.getLastModifiedDate(), ISO_DATE_TIME.format(client.getLastModifiedDate()));
	}

	@Test
	public void givenClientsListWhenMapToDtosThenCorrectValuesMapped() {

		// setup
		Client c1 = ClientTestFactory.createClient("c1");
		Client c2 = ClientTestFactory.createClient("c2");
		List<Client> clients = new ArrayList<>();
		clients.add(c1);
		clients.add(c2);

		// execute
		List<ClientDto> clientDtos = mapper.mapEntityListToDtoList(clients);

		// verify
		IntStream.range(0, clientDtos.size())
				.forEach(i -> assertEqualValues(clients.get(i), clientDtos.get(i)));
	}

	@Test
	public void givenDtosListWhenMapToClientsThenCorrectValuesMapped() {

		// setup
		ClientDto clientDto1 = ClientDtoTestFactory.createClientDto(1L);
		ClientDto clientDto2 = ClientDtoTestFactory.createClientDto(2L);
		List<ClientDto> clientDtos = new ArrayList<>();
		clientDtos.add(clientDto1);
		clientDtos.add(clientDto2);

		// execute
		List<Client> clients = mapper.mapDtoListToEntityList(clientDtos);

		// verify

		IntStream.range(0, clientDtos.size())
				.forEach(i -> assertEqualValues(clientDtos.get(i), clients.get(i)));
	}
}