package com.monese;

import com.monese.model.Account;
import com.monese.model.Client;
import com.monese.provisioning.AccountProvisioner;
import com.monese.provisioning.ClientProvisioner;
import com.monese.provisioning.TransactionProvisioner;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.eclipse.jetty.servlet.ServletContextHandler.NO_SESSIONS;

/**
 * Configuration class used to start an embedded jetty server with required properties for
 * listening on jersey endpoints
 *
 * @author acozma
 */
public class JerseyApplication {

    private static final Logger logger = LoggerFactory.getLogger(JerseyApplication.class);

    public static void main(String[] args) {

        Server server = new Server(8080);

        ServletContextHandler servletContextHandler = new ServletContextHandler(NO_SESSIONS);

        servletContextHandler.setContextPath("/");
        server.setHandler(servletContextHandler);

        ServletHolder servletHolder = servletContextHandler.addServlet(ServletContainer.class, "/api/*");
        servletHolder.setInitOrder(0);
        servletHolder.setInitParameter(
                "jersey.config.server.provider.packages",
                "com.monese.api.resources"
        );

        try {

            createDemoData();

            server.start();
            server.join();
        } catch (Exception ex) {
            logger.error("Error occurred while starting Jetty", ex);
            System.exit(1);
        } finally {
            server.destroy();
        }
    }

    private static void createDemoData() {

        List<Client> clients = ClientProvisioner.provisionClients();
        List<Account> accounts = AccountProvisioner.provisionAccounts(clients);
        TransactionProvisioner.provisionTransactions(accounts);
    }
}
