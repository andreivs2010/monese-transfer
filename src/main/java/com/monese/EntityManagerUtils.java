package com.monese;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Utils class for operations with {@link EntityManager}
 *
 * @author acozma
 */
public class EntityManagerUtils {

	private static final String PERSISTENT_UNIT_NAME = "easyTransfer";

	private static EntityManagerFactory emf;

	/**
	 * Get entity manager for current persistence unit
	 *
	 * @return the entity manager for current persistence unit
	 */
	public static EntityManager getEntityManager() {

		if (emf == null || !emf.isOpen()) {
			emf = Persistence.createEntityManagerFactory(PERSISTENT_UNIT_NAME);
		}
		return emf.createEntityManager();
	}

	/**
	 * Closes the entity manager for current persistence unit
	 */
	public static void closeEntityManagerFactory() {

		if (emf != null && emf.isOpen()) {
			emf.close();
		}
	}
}
