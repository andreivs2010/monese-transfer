package com.monese.service;

import com.monese.api.dto.TransactionDto;
import com.monese.dao.EntityDao;
import com.monese.dao.exception.TransactionNotFoundException;
import com.monese.dao.impl.TransactionDaoImpl;
import com.monese.mapper.TransactionMapperEnhancer;
import com.monese.model.Transaction;

import java.util.List;

/**
 * Service class for {@link Transaction}
 *
 * @author acozma
 */
public class TransactionService {

	private EntityDao<Transaction> dao;

	private TransactionMapperEnhancer mapper;

	private TransactionService(
			final EntityDao<Transaction> dao, final TransactionMapperEnhancer mapper) {

		this.dao = dao;

		this.mapper = mapper;
	}

	private static class Singleton {

		private static final TransactionService INSTANCE = new TransactionService(TransactionDaoImpl.getInstance(),
				TransactionMapperEnhancer.getInstance());

	}

	public static TransactionService getInstance() {

		return Singleton.INSTANCE;
	}

	public List<TransactionDto> getAll() {

		List<Transaction> transactions = dao.getAllEntities();
		return mapper.mapEntityListToDtoList(transactions);
	}

	public TransactionDto getById(final long id) {

		Transaction entity = dao.getEntity(id);
		if (entity == null) {
			throw new TransactionNotFoundException();
		}
		return mapper.mapEntityToDto(entity);
	}

	public List<TransactionDto> getByAccountId(final long id) {

		TransactionDaoImpl dao = (TransactionDaoImpl) this.dao;

		List<Transaction> entities = dao.getByAccountId(id);

		if (entities == null) {
			throw new TransactionNotFoundException();
		}
		return mapper.mapEntityListToDtoList(entities);
	}

	public TransactionDto create(final TransactionDto input) {

		Transaction entity = mapper.mapDtoToEntity(input);
		dao.storeAndTouchEntity(entity);
		return mapper.mapEntityToDto(entity);
	}

	public TransactionDto update(final long id, final TransactionDto transactionDto) {

		Transaction entityToUpdate = dao.getEntity(id);
		if (entityToUpdate == null) {
			throw new TransactionNotFoundException();
		}
		Transaction transactionFromInput = mapper.mapDtoToEntity(transactionDto);

		updateEntityFromDto(entityToUpdate, transactionFromInput);

		dao.updateAndTouchEntity(entityToUpdate);

		return mapper.mapEntityToDto(entityToUpdate);
	}

	private void updateEntityFromDto(final Transaction entityToUpdate, final Transaction transactionFromInput) {

		entityToUpdate.setParentAccount(transactionFromInput.getParentAccount());
		entityToUpdate.setTransactionId(transactionFromInput.getTransactionId());
		entityToUpdate.setCreationDate(transactionFromInput.getCreationDate());
		entityToUpdate.setParentTransactionLinkId(transactionFromInput.getParentTransactionLinkId());
		entityToUpdate.setChildTransactionLinkId(transactionFromInput.getChildTransactionLinkId());
		entityToUpdate.setAmount(transactionFromInput.getAmount());
		entityToUpdate.setType(entityToUpdate.getType());
	}

	public void delete(final long id) {

		dao.delete(id);
	}
}
