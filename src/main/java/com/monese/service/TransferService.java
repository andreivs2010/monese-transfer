package com.monese.service;

import com.monese.api.dto.TransferDetailsDto;
import com.monese.dao.impl.AccountDaoImpl;
import com.monese.dao.impl.TransactionDaoImpl;
import com.monese.model.Account;
import com.monese.model.Transaction;
import com.monese.service.exception.AmountValidationException;
import com.monese.service.exception.TransferException;

import java.math.BigDecimal;

/**
 * Service class for executing transaction actions
 *
 * @author acozma
 */
public class TransferService {

	private AmountValidationService amountValidationService;

	private AccountDaoImpl accountDao;

	private TransactionDaoImpl transactionDao;

	private TransferService(final AmountValidationService amountValidationService,
							final AccountDaoImpl accountDao, final TransactionDaoImpl transactionDao) {

		this.amountValidationService = amountValidationService;
		this.accountDao = accountDao;
		this.transactionDao = transactionDao;
	}

	private static class Singleton {

		private final static TransferService INSTANCE = new TransferService(AmountValidationService.getInstance(),
				AccountDaoImpl.getInstance(), TransactionDaoImpl.getInstance());
	}

	public static TransferService getInstance() {
		return Singleton.INSTANCE;
	}

	/**
	 * Performs the transfer action between two accounts
	 *
	 * @param sourceAccountId
	 * 		the id of the {@link Account} from which money is transferred
	 * @param transferDetails
	 * 		contains the target {@link Account} and the amount
	 */
	public void transfer(Long sourceAccountId, TransferDetailsDto transferDetails)
			throws AmountValidationException, TransferException {

		amountValidationService.validate(transferDetails.getAmount());

		Account sourceAccount = accountDao.getEntity(sourceAccountId);

		if (sourceAccount == null) {
			throw new TransferException(TransferException.Reason.MISSING_SOURCE_ACCOUNT);
		}

		Long targetAccountId = transferDetails.getTargetAccountId();
		Account targetAccount = accountDao.getEntity(targetAccountId);

		if (targetAccount == null) {
			throw new TransferException(TransferException.Reason.MISSING_TARGET_ACCOUNT);
		}

		if(sourceAccountId.equals(targetAccountId)) {
			throw new TransferException(TransferException.Reason.SAME_ACCOUNT);
		}

		BigDecimal balance = sourceAccount.getBalance();

		if (balance.compareTo(transferDetails.getAmount()) < 0) {
			throw new TransferException(TransferException.Reason.INSUFICIENT_FUNDS);
		}

		makeTransfer(sourceAccount, targetAccount, transferDetails.getAmount());
	}

	private void makeTransfer(Account sourceAccount, Account targetAccount, BigDecimal amount) {

		//TODO - Refactor this method so that the transaction is committed only at the end of the method
		//the current problem is that individual transactions are committed and this could lead to
		//inconsistent data

		Transaction sourceTransaction = new Transaction();
		sourceTransaction.setAmount(amount);
		sourceTransaction.setParentAccount(sourceAccount);
		sourceTransaction.setType(Transaction.TransactionType.TRANSFER);
		transactionDao.storeAndTouchEntity(sourceTransaction);

		Transaction targetTransaction = new Transaction();
		targetTransaction.setAmount(amount);
		targetTransaction.setParentAccount(targetAccount);
		targetTransaction.setType(Transaction.TransactionType.TRANSFER);
		targetTransaction.setParentTransactionLinkId(sourceTransaction.getId());
		transactionDao.storeAndTouchEntity(targetTransaction);

		sourceTransaction.setChildTransactionLinkId(targetTransaction.getId());
		transactionDao.updateAndTouchEntity(sourceTransaction);

		sourceAccount.subtractAmount(amount);

		targetAccount.addAmount(amount);
		accountDao.updateAndTouchEntity(sourceAccount);
		accountDao.updateAndTouchEntity(targetAccount);
	}
}
