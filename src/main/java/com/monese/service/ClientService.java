package com.monese.service;

import com.monese.api.dto.AccountDto;
import com.monese.api.dto.ClientDto;
import com.monese.dao.EntityDao;
import com.monese.dao.exception.ClientNotFoundException;
import com.monese.dao.impl.ClientDaoImpl;
import com.monese.mapper.AccountMapper;
import com.monese.mapper.ClientMapper;
import com.monese.model.Client;

import java.util.List;

/**
 * Service class for {@link Client}
 *
 * @author acozma
 */
public class ClientService {


    private EntityDao<Client> clientDao;

    private ClientMapper mapper;
    private AccountMapper accountMapper;

    private ClientService(ClientDaoImpl clientDao, final ClientMapper mapper,
                          final AccountMapper accountMapper) {

        this.clientDao = clientDao;
        this.mapper = mapper;
        this.accountMapper = accountMapper;
    }

    private static class Singleton {
        private final static ClientService INSTANCE = new ClientService(ClientDaoImpl.getInstance(), ClientMapper.getInstance(),
                AccountMapper.getInstance());
    }

    public static ClientService getInstance() {

        return Singleton.INSTANCE;
    }

    public ClientDto storeClient(ClientDto clientDto) {

        Client client = mapper.mapDtoToEntity(clientDto);

        clientDao.storeAndTouchEntity(client);

        return mapper.mapEntityToDto(client);

    }

    public ClientDto updateClient(final long id, ClientDto clientDto) throws ClientNotFoundException {

        Client clientToUpdate = clientDao.getEntity(id);

        if (clientToUpdate == null) {
            throw new ClientNotFoundException();
        }

        // TODO obtain a NEW client from mapper won't work on update because it was never created
        clientToUpdate.setName(clientDto.getName());
        clientToUpdate.setState(Client.ClientState.valueOf(clientDto.getState().name()));
        clientToUpdate.setAddress(clientDto.getAddress());
        clientToUpdate.setNumericPersonalCode(clientDto.getNumericPersonalCode());
        clientToUpdate.setPhoneNumber(clientDto.getPhoneNumber());

        clientDao.updateAndTouchEntity(clientToUpdate);

        return mapper.mapEntityToDto(clientToUpdate);
    }

    public ClientDto getClient(Long id) {

        Client clientFromDb = getClientFromDb(id);

        return mapper.mapEntityToDto(clientFromDb);
    }

    public List<ClientDto> getAllClients() {

        List<Client> allClients = clientDao.getAllEntities();

        return mapper.mapEntityListToDtoList(allClients);

    }

    public void deleteClient(Long id) throws ClientNotFoundException {

        clientDao.delete(id);

    }

    private Client getClientFromDb(Long id) {

        return clientDao.getEntity(id);

    }

    public List<AccountDto> getAccountsOfClientBy(final long id) {

        Client entity = clientDao.getEntity(id);

        return accountMapper.mapEntityListToDtoList(entity.getAccounts());
    }
}
