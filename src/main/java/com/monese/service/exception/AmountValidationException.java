package com.monese.service.exception;

/**
 * Checked exception which should be thrown when trying to transfer an invalid amount
 *
 * @author acozma
 */
public class AmountValidationException extends Exception {

	public enum Reason {
		INCORRECT_AMOUNT
	}

	private Reason reason;

	public AmountValidationException(Reason reason) {

		this.reason = reason;
	}

	public Reason getReason() {

		return reason;
	}
}
