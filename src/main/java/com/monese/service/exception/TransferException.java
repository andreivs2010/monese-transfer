package com.monese.service.exception;

/**
 * Checked exception which should be thrown when executing an invalid transfer
 *
 * @author acozma
 */
public class TransferException extends Exception {

	public enum Reason {
		MISSING_SOURCE_ACCOUNT,

		MISSING_TARGET_ACCOUNT,

		INSUFICIENT_FUNDS,

		SAME_ACCOUNT
	}

	private Reason reason;

	public TransferException(Reason reason) {

		this.reason = reason;
	}

	public Reason getReason() {

		return reason;
	}
}
