package com.monese.service;

import com.monese.service.exception.AmountValidationException;

import java.math.BigDecimal;

/**
 * Service class for validating the transferred amount
 *
 * @author acozma
 */
public class AmountValidationService {

	private AmountValidationService() {

	}

	private static class Singleton {
		private final static AmountValidationService INSTANCE = new AmountValidationService();
	}

	public static AmountValidationService getInstance() {

		return Singleton.INSTANCE;
	}

	/**
	 * Validate the transfer amount
	 *
	 * @param amount
	 * 		the transfer amount
	 */
	public void validate(BigDecimal amount) throws AmountValidationException {

		if (amount.compareTo(BigDecimal.ZERO) <= 0) {
			throw new AmountValidationException(AmountValidationException.Reason.INCORRECT_AMOUNT);
		}

	}
}
