package com.monese.service;

import com.monese.api.dto.AccountDto;
import com.monese.dao.exception.AccountNotFoundException;
import com.monese.dao.impl.AccountDaoImpl;
import com.monese.mapper.AccountMapper;
import com.monese.model.Account;
import com.monese.model.Transaction;

import java.util.List;

/**
 * @author acozma
 */
public class AccountService {

	private AccountDaoImpl accountDao;
	private AccountMapper mapper;

	private AccountService(AccountDaoImpl accountDao,
						   final AccountMapper mapper) {

		this.accountDao = accountDao;
		this.mapper = mapper;
	}

	private static class Singleton {
		private final static AccountService INSTANCE = new AccountService(AccountDaoImpl.getInstance(),
				AccountMapper.getInstance());
	}

	public static AccountService getInstance() {

		return Singleton.INSTANCE;
	}

	/**
	 * Store new {@link Account} entity
	 *
	 * @param accountDto
	 * 		- data transfer object containing information for the new {@link Account}
	 */
	public void storeAccount(AccountDto accountDto) {

		Account account = mapper.mapDtoToEntity(accountDto);
		account.touch();
		accountDao.storeAndTouchEntity(account);
	}

	/**
	 * @param accountDto
	 *
	 * @throws AccountNotFoundException
	 */
	public void updateAccount(AccountDto accountDto) throws AccountNotFoundException {

		Account account = accountDao.getEntity(accountDto.getAccountId());

		if (account == null) {
			throw new AccountNotFoundException();
		}

		List<Transaction> transactions = account.getTransactions();

		account = mapper.mapDtoToEntity(accountDto);

		account.setTransactions(transactions);

		account.touch();

		accountDao.updateAndTouchEntity(account);

	}

	/**
	 * @param id
	 *
	 * @return
	 */
	public AccountDto getAccount(Long id) {

		Account accountFromDb = getAccountFromDb(id);

		return mapper.mapEntityToDto(accountFromDb);
	}

	/**
	 * @return
	 */
	public List<AccountDto> getAllAccounts() {

		List<Account> allAccounts = accountDao.getAllEntities();

		return mapper.mapEntityListToDtoList(allAccounts);

	}

	/**
	 * @param id
	 *
	 * @throws AccountNotFoundException
	 */
	public void deleteAccount(Long id) throws AccountNotFoundException {

		accountDao.delete(id);

	}

	/**
	 * @param id
	 *
	 * @return
	 */
	private Account getAccountFromDb(Long id) {

		return accountDao.getEntity(id);

	}

}
