package com.monese.mapper;

import com.monese.api.dto.ClientDto;
import com.monese.model.Account;
import com.monese.model.Client;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static java.time.LocalDateTime.parse;
import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;

/**
 * Mapper which converts {@link Client} entities to {@link ClientDto} and vice-versa
 *
 * @author acozma
 */
public class ClientMapper implements Mapper<Client, ClientDto> {

    private ClientMapper() {

    }

    private static class Singleton {
        private final static ClientMapper INSTANCE = new ClientMapper();
    }

    public static ClientMapper getInstance() {

        return Singleton.INSTANCE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ClientDto mapEntityToDto(Client entity) {

        ClientDto clientDto = new ClientDto();
        clientDto.setClientId(entity.getClientId());
        clientDto.setName(entity.getName());
        clientDto.setAddress(entity.getAddress());

        LocalDateTime creationDate = entity.getCreationDate();
        if (creationDate != null) {
            clientDto.setCreationDate(ISO_DATE_TIME.format(creationDate));
        }

        if (entity.getLastModifiedDate() != null) {
            clientDto.setLastModifiedDate(ISO_DATE_TIME.format(entity.getLastModifiedDate()));
        }
        clientDto.setNumericPersonalCode(entity.getNumericPersonalCode());
        clientDto.setPhoneNumber(entity.getPhoneNumber());
        List<Long> accountIds = entity.getAccounts().stream().map(Account::getAccountId).collect(Collectors.toList());
        clientDto.setAccountIds(accountIds);
        clientDto.setState(ClientDto.ClientStateDto.valueOf(entity.getState().name()));
        return clientDto;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Client mapDtoToEntity(ClientDto clientDto) {

        Client client = new Client();
        client.setName(clientDto.getName());

        client.setState(Client.ClientState.valueOf(clientDto.getState().name()));
        client.setAddress(clientDto.getAddress());
        client.setPhoneNumber(clientDto.getPhoneNumber());
        client.setNumericPersonalCode(clientDto.getNumericPersonalCode());

        String creationDate = clientDto.getCreationDate();
        if (creationDate != null && !creationDate.isEmpty()) {
            client.setCreationDate(parse(creationDate, ISO_DATE_TIME));
        }

        String lastModifiedDate = clientDto.getLastModifiedDate();
        if (lastModifiedDate != null && !lastModifiedDate.isEmpty()) {
            client.setLastModifiedDate(parse(lastModifiedDate, ISO_DATE_TIME));
        }
        return client;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ClientDto> mapEntityListToDtoList(List<Client> clients) {

        return clients.stream()
                .map(this::mapEntityToDto)
                .collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Client> mapDtoListToEntityList(List<ClientDto> clientDtos) {

        return clientDtos.stream()
                .map(this::mapDtoToEntity)
                .collect(Collectors.toList());
    }
}
