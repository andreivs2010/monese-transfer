package com.monese.mapper;

import com.monese.api.dto.AccountDto;
import com.monese.model.Account;
import com.monese.model.Transaction;

import java.util.List;

import static java.time.LocalDateTime.parse;
import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;
import static java.util.stream.Collectors.toList;

/**
 * Mapper which converts {@link Account} entities to {@link AccountDto} and vice-versa
 *
 * @author acozma
 */
public class AccountMapper implements Mapper<Account, AccountDto> {


	private AccountMapper() {

	}

	private static class Singleton {
		private final static AccountMapper INSTANCE = new AccountMapper();
	}

	public static AccountMapper getInstance() {

		return Singleton.INSTANCE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AccountDto mapEntityToDto(Account entity) {

		AccountDto dto = new AccountDto();

		dto.setAccountHolderId(entity.getAccountHolder().getId());
		dto.setAccountId(entity.getId());
		dto.setBalance(entity.getBalance());
		dto.setCreationDate(ISO_DATE_TIME.format(entity.getCreationDate()));
		dto.setLastModifiedDate(ISO_DATE_TIME.format(entity.getLastModifiedDate()));
		dto.setState(AccountDto.AccountStateDto.valueOf(entity.getState().name()));

		List<Long> transactionIds = entity.getTransactions().stream()
				.map(Transaction::getId)
				.collect(toList());
		dto.setTransactionIds(transactionIds);

		return dto;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Account mapDtoToEntity(AccountDto dto) {

		Account entity = new Account();
		entity.setAccountId(dto.getAccountId());
		entity.setState(Account.AccountState.valueOf(dto.getState().name()));
		entity.setCreationDate(parse(dto.getCreationDate(), ISO_DATE_TIME));
		entity.setLastModifiedDate(parse(dto.getLastModifiedDate(), ISO_DATE_TIME));
		entity.setBalance(dto.getBalance());
		return entity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AccountDto> mapEntityListToDtoList(List<Account> entities) {

		return entities.stream()
				.map(this::mapEntityToDto)
				.collect(toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Account> mapDtoListToEntityList(List<AccountDto> dtos) {

		return dtos.stream()
				.map(this::mapDtoToEntity)
				.collect(toList());
	}
}
