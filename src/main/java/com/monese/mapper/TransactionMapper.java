package com.monese.mapper;

import com.monese.api.dto.TransactionDto;
import com.monese.model.Transaction;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Mapper which converts {@link Transaction} entities to {@link TransactionDto} and vice-versa
 *
 * @author acozma
 */
public class TransactionMapper implements Mapper<Transaction, TransactionDto> {

	private TransactionMapper() {

	}

	private static class Singleton {
		private final static TransactionMapper INSTANCE = new TransactionMapper();
	}

	public static TransactionMapper getInstance() {

		return Singleton.INSTANCE;
	}

	@Override
	public TransactionDto mapEntityToDto(Transaction entity) {

		TransactionDto dto = new TransactionDto();

		dto.setAmount(entity.getAmount());
		dto.setChildTransactionLinkId(entity.getChildTransactionLinkId());
		dto.setParentTransactionLinkId(entity.getParentTransactionLinkId());
		dto.setTransactionId(entity.getTransactionId());

		return dto;
	}

	@Override
	public Transaction mapDtoToEntity(TransactionDto dto) {

		Transaction entity = new Transaction();

		entity.setAmount(dto.getAmount());
		entity.setChildTransactionLinkId(dto.getChildTransactionLinkId());
		entity.setParentTransactionLinkId(dto.getParentTransactionLinkId());
		entity.setTransactionId(dto.getTransactionId());

		return entity;
	}

	@Override
	public List<TransactionDto> mapEntityListToDtoList(List<Transaction> entities) {

		return entities.stream()
				.map(this::mapEntityToDto)
				.collect(toList());
	}

	@Override
	public List<Transaction> mapDtoListToEntityList(List<TransactionDto> dtos) {

		return dtos.stream()
				.map(this::mapDtoToEntity)
				.collect(toList());
	}
}
