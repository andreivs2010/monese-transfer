package com.monese.mapper;

import java.util.List;

/**
 * Abstract mapper used to map entities to dtos and vice-versa
 *
 * @author acozma
 */
public interface Mapper<E, D> {

	/**
	 * Map an entity to the equivalent dto object
	 *
	 * @param entity
	 * 		- the entity
	 *
	 * @return the dto
	 */
	D mapEntityToDto(E entity);

	/**
	 * Map a dto to the equivalent entity object
	 *
	 * @param dto
	 * 		- the dto
	 *
	 * @return the entity
	 */
	E mapDtoToEntity(D dto);

	/**
	 * Map a list of entities to the equivalent list of dto objects
	 *
	 * @param entities
	 * 		- the list of entities
	 *
	 * @return the list of dto objects
	 */
	List<D> mapEntityListToDtoList(List<E> entities);

	/**
	 * Map a list of dtos to the equivalent list of entities
	 *
	 * @param dtos
	 * 		- the list of dtos
	 *
	 * @return the list of entity objects
	 */
	List<E> mapDtoListToEntityList(List<D> dtos);
}
