package com.monese.mapper;

import com.monese.api.dto.TransactionDto;
import com.monese.dao.impl.AccountDaoImpl;
import com.monese.model.Account;
import com.monese.model.Transaction;
import com.monese.model.Transaction.TransactionType;

import java.util.List;

import static com.monese.api.dto.TransactionDto.TransactionTypeDto.valueOf;
import static java.time.LocalDateTime.parse;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;
import static java.util.stream.Collectors.toList;

public class TransactionMapperEnhancer implements Mapper<Transaction, TransactionDto> {

	private final Mapper<Transaction,TransactionDto> mapper;
	private final AccountDaoImpl accountDao;

	private TransactionMapperEnhancer(TransactionMapper mapper,
									  final AccountDaoImpl accountDao) {

		this.mapper = mapper;
		this.accountDao = accountDao;
	}

	private static class Singleton {

		private final static TransactionMapperEnhancer INSTANCE = new TransactionMapperEnhancer(
				TransactionMapper.getInstance(), AccountDaoImpl.getInstance());
	}

	public static TransactionMapperEnhancer getInstance() {

		return Singleton.INSTANCE;
	}

	@Override
	public TransactionDto mapEntityToDto(final Transaction entity) {

		TransactionDto dto = mapper.mapEntityToDto(entity);

		dto.setParentAccountId(entity.getParentAccount().getId());
		dto.setType(valueOf(entity.getType().name()));
		dto.setCreationDate(ISO_LOCAL_DATE_TIME.format(entity.getCreationDate()));

		return dto;
	}

	@Override
	public Transaction mapDtoToEntity(final TransactionDto dto) {

		Transaction entity = mapper.mapDtoToEntity(dto);

		Account account = accountDao.getEntity(dto.getParentAccountId());
		entity.setParentAccount(account);

		entity.setType(TransactionType.valueOf(dto.getType().name()));

		String creationDate = dto.getCreationDate();
		if(creationDate !=null && !creationDate.isEmpty()) {
			entity.setCreationDate(parse(creationDate,ISO_LOCAL_DATE_TIME));
		}

		return entity;
	}

	@Override
	public List<TransactionDto> mapEntityListToDtoList(final List<Transaction> entities) {

		return entities.stream()
				.map(this::mapEntityToDto)
				.collect(toList());
	}

	@Override
	public List<Transaction> mapDtoListToEntityList(final List<TransactionDto> dtos) {

		return dtos.stream()
				.map(this::mapDtoToEntity)
				.collect(toList());
	}
}
