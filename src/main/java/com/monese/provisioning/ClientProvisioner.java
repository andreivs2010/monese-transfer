package com.monese.provisioning;

import com.monese.dao.impl.ClientDaoImpl;
import com.monese.model.Client;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.monese.model.Client.ClientState.ACTIVE;

/**
 * Provision {@link Client} entities whether persisted in a persistence unit or not
 *
 * @author acozma
 */
public class ClientProvisioner {

	/**
	 * Provision persisted {@link Client} entities
	 *
	 * @return the list of persisted {@link Client} objects
	 */
	public static List<Client> provisionClients() {

		Client sherlock = createClient("Sherlock Holmes");
		Client watson = createClient("John Watson");

		ClientDaoImpl.getInstance().storeAndTouchEntity(sherlock);
		ClientDaoImpl.getInstance().storeAndTouchEntity(watson);

		List<Client> clients = new ArrayList<>();
		clients.add(sherlock);
		clients.add(watson);

		return clients;
	}

	/**
	 * Provision new {@link Client} with given name
	 *
	 * @param name
	 * 		- given {@link Client#name}
	 *
	 * @return the {@link Client}
	 */
	private static Client createClient(final String name) {

		Client client = new Client();
		client.setName(name);
		client.setAddress("221B Backer Street");
		client.setNumericPersonalCode("19272183126");
		client.setPhoneNumber("0747356347");
		client.setAccounts(new ArrayList<>());
		client.setState(ACTIVE);
		client.setCreationDate(LocalDateTime.now());
		client.setLastModifiedDate(LocalDateTime.now());

		return client;
	}
}
