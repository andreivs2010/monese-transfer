package com.monese.provisioning;

import com.monese.dao.impl.TransactionDaoImpl;
import com.monese.model.Account;
import com.monese.model.Transaction;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static com.monese.model.Transaction.TransactionType.TRANSFER;

/**
 * Provision {@link Transaction} entities whether persisted in a persistence unit or not
 *
 * @author acozma
 */
public class TransactionProvisioner {

	private static final LocalDateTime LDT_2019_11_03_14_30_22 = LocalDateTime.of(2019, 11, 3, 14, 30, 22);
	private static final BigDecimal BD_10_00 = new BigDecimal("10.00");

	/**
	 * Provision persisted {@link Transaction} entities
	 */
	public static void provisionTransactions(List<Account> accounts) {

		accounts.forEach(account -> {
			TransactionDaoImpl dao = TransactionDaoImpl.getInstance();
			Transaction t1 = createTransaction(account);
			dao.storeEntity(t1);
			Transaction t2 = createTransaction(account);
			dao.storeEntity(t2);
			t1.setChildTransactionLinkId(t2.getId());
			t2.setParentTransactionLinkId(t1.getId());
			dao.updateEntity(t1);
			dao.updateEntity(t2);


		});
	}

	private static Transaction createTransaction(final Account account) {
		Transaction transaction = new Transaction();
		transaction.setAmount(BD_10_00);
		transaction.setCreationDate(LDT_2019_11_03_14_30_22);
		transaction.setParentAccount(account);
		transaction.setType(TRANSFER);
		return transaction;
	}

}
