package com.monese.provisioning;

import com.monese.dao.impl.AccountDaoImpl;
import com.monese.model.Account;
import com.monese.model.Client;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.monese.model.Account.AccountState.INACTIVE;

/**
 * Provision {@link Account} entities whether persisted in a persistence unit or not
 *
 * @author acozma
 */
public class AccountProvisioner {

	private static final LocalDateTime LDT_2019_11_03_14_30_22 = LocalDateTime.of(2019, 11, 3, 14, 30, 22);
	private static final BigDecimal BD_10_00 = new BigDecimal("10.00");

	/**
	 * Provision persisted {@link Account} objects for given clients
	 *
	 * @param clients
	 * 		- list of clients
	 * @return
	 */
	public static List<Account> provisionAccounts(final List<Client> clients) {

		List<Account> accounts = new ArrayList<>();
		clients.forEach(client -> {
			Account account1 = createAccount(client);
			AccountDaoImpl.getInstance().storeEntity(account1);
			Account account2 = createAccount(client);
			AccountDaoImpl.getInstance().storeEntity(account2);
			accounts.add(account1);
			accounts.add(account2);
		});
		return accounts;
	}

	/**
	 * Provision new {@link Account}  for given client
	 *
	 * @param accountHolder
	 * 		- the account holder
	 *
	 * @return the new account
	 */
	private static Account createAccount(Client accountHolder) {

		Account account = new Account();

		account.setAccountHolder(accountHolder);
		account.setBalance(BD_10_00);
		account.setCreationDate(LDT_2019_11_03_14_30_22);
		account.setLastModifiedDate(LDT_2019_11_03_14_30_22);
		account.setState(INACTIVE);
		account.setTransactions(new ArrayList<>());

		return account;
	}

}
