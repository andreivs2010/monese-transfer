package com.monese.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Entity describing the attributes of a client
 *
 * @author acozma
 */
@Entity
@Table(name = "client")
public class Client implements HasId, IsTouchable {

	public enum ClientState {

		ACTIVE, INACTIVE, BLACKLISTED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long clientId;

	private String name;

	private ClientState state;

	private String numericPersonalCode;

	private String phoneNumber;

	private String address;

	private LocalDateTime creationDate;

	private LocalDateTime lastModifiedDate;

	@Override
	public Long getId() {

		return clientId;
	}

	@OneToMany(mappedBy = "accountHolder", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Account> accounts;

	public Client() {

		this.accounts = new ArrayList<>();
	}

	public Client(String name, ClientState state, String numericPersonalCode, String phoneNumber,
				  String address, LocalDateTime creationDate, LocalDateTime lastModifiedDate, List<Account> accounts) {

		this.name = name;
		this.state = state;
		this.numericPersonalCode = numericPersonalCode;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.creationDate = creationDate;
		this.lastModifiedDate = lastModifiedDate;
		this.accounts = accounts;
	}

	/**
	 * Update date information for the {@link Client}
	 * <p>
	 * //TODO Implement entity listener which treats this update
	 */
	@Override
	public void touch() {

		if (creationDate == null) {
			this.creationDate = LocalDateTime.now();
		}
		this.lastModifiedDate = LocalDateTime.now();
	}

	public void addAccount(Account account) {

		if (this.accounts == null) {
			this.accounts = new ArrayList<>();
		}
		this.accounts.add(account);
	}

	public Long getClientId() {

		return clientId;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public ClientState getState() {

		return state;
	}

	public void setState(ClientState state) {

		this.state = state;
	}

	public String getNumericPersonalCode() {

		return numericPersonalCode;
	}

	public void setNumericPersonalCode(String numericPersonalCode) {

		this.numericPersonalCode = numericPersonalCode;
	}

	public String getPhoneNumber() {

		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {

		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {

		return address;
	}

	public void setAddress(String address) {

		this.address = address;
	}

	public List<Account> getAccounts() {

		return accounts;
	}

	public void setAccounts(List<Account> accounts) {

		this.accounts = accounts;
	}

	public LocalDateTime getCreationDate() {

		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {

		this.creationDate = creationDate;
	}

	public LocalDateTime getLastModifiedDate() {

		return lastModifiedDate;
	}

	public void setLastModifiedDate(LocalDateTime lastModifiedDate) {

		this.lastModifiedDate = lastModifiedDate;
	}
}
