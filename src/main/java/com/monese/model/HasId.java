package com.monese.model;

/**
 * Marker interface for all entities with id
 */
public interface HasId {

    /**
     * Retrieve the id
     */
    Long getId();
}
