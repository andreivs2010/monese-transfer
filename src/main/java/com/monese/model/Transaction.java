package com.monese.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Entity class describing the attributes of a transaction of an account
 *
 * @author acozma
 */
@Entity
@Table(name = "transaction")
public class Transaction implements HasId, IsTouchable {

	public enum TransactionType {

		TRANSFER
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long transactionId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "accountId")
	private Account parentAccount;

	private Long parentTransactionLinkId;

	private Long childTransactionLinkId;

	private TransactionType type;

	private BigDecimal amount;

	private LocalDateTime creationDate;

	/**
	 * Constructor used for serialization
	 */
	public Transaction() {

	}

	@Override
	public void touch() {

		if (creationDate == null) {
			this.creationDate = LocalDateTime.now();
		}
	}

	@Override
	public Long getId() {

		return transactionId;
	}

	public Long getTransactionId() {

		return transactionId;
	}

	public void setTransactionId(Long id) {

		this.transactionId = id;
	}

	public Account getParentAccount() {

		return parentAccount;
	}

	public void setParentAccount(Account parentAccount) {

		this.parentAccount = parentAccount;
	}

	public TransactionType getType() {

		return type;
	}

	public void setType(TransactionType type) {

		this.type = type;
	}

	public BigDecimal getAmount() {

		return amount;
	}

	public void setAmount(BigDecimal amount) {

		this.amount = amount;
	}

	public LocalDateTime getCreationDate() {

		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {

		this.creationDate = creationDate;
	}

	public Long getParentTransactionLinkId() {

		return parentTransactionLinkId;
	}

	public void setParentTransactionLinkId(Long parentTransactionLinkId) {

		this.parentTransactionLinkId = parentTransactionLinkId;
	}

	public Long getChildTransactionLinkId() {

		return childTransactionLinkId;
	}

	public void setChildTransactionLinkId(Long childTransactionLinkId) {

		this.childTransactionLinkId = childTransactionLinkId;
	}
}
