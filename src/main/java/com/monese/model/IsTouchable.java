package com.monese.model;

/**
 * Marker interface for all entities with date fields
 */
public interface IsTouchable {

    /**
     * Set date fields
     */
    void touch();
}
