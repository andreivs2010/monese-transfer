package com.monese.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Entity class describing the attributes of an account
 *
 * @author acozma
 */
@Entity
@Table(name = "account")
public class Account implements HasId, IsTouchable {

	public enum AccountState {

		ACTIVE,

		INACTIVE,

		REJECTED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long accountId;

	private BigDecimal balance;

	private AccountState state;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "client_clientId")
	private Client accountHolder;

	@OneToMany(mappedBy = "parentAccount", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Transaction> transactions;

	private LocalDateTime creationDate;

	private LocalDateTime lastModifiedDate;

	public Account() {

	}

	public Account(BigDecimal balance, AccountState state, Client accountHolder, List<Transaction> transactions,
				   LocalDateTime creationDate, LocalDateTime lastModifiedDate) {

		this.balance = balance;
		this.state = state;
		this.accountHolder = accountHolder;
		this.transactions = transactions;
		this.creationDate = creationDate;
		this.lastModifiedDate = lastModifiedDate;
	}

	/**
	 * Update date information for the {@link Client}
	 * <p>
	 * //TODO Implement entity listener which treats this update
	 */
	@Override
	public void touch() {

		if (creationDate == null) {
			this.creationDate = LocalDateTime.now();
		}
		this.lastModifiedDate = LocalDateTime.now();
	}

	@Override
	public Long getId() {

		return accountId;
	}

	public Long getAccountId() {

		return accountId;
	}

	public void setAccountId(Long accountId) {

		this.accountId = accountId;
	}

	public BigDecimal getBalance() {

		return balance;
	}

	public void setBalance(BigDecimal balance) {

		this.balance = balance;
	}

	public AccountState getState() {

		return state;
	}

	public void setState(AccountState state) {

		this.state = state;
	}

	public Client getAccountHolder() {

		return accountHolder;
	}

	public void setAccountHolder(Client accountHolder) {

		this.accountHolder = accountHolder;
	}

	public List<Transaction> getTransactions() {

		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {

		this.transactions = transactions;
	}

	public LocalDateTime getCreationDate() {

		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {

		this.creationDate = creationDate;
	}

	public LocalDateTime getLastModifiedDate() {

		return lastModifiedDate;
	}

	public void setLastModifiedDate(LocalDateTime lastModifiedDate) {

		this.lastModifiedDate = lastModifiedDate;
	}

	public void addTransaction(Transaction transaction) {

		if (transactions == null) {
			transactions = new ArrayList<>();
		}
		transactions.add(transaction);
	}

	public void subtractAmount(BigDecimal amount) {

		this.balance = this.balance.subtract(amount);
	}

	public void addAmount(BigDecimal amount) {

		this.balance = this.balance.add(amount);
	}
}
