package com.monese.api.resources;

import com.monese.api.dto.TransactionDto;
import com.monese.dao.exception.TransactionNotFoundException;
import com.monese.service.TransactionService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

import static javax.ws.rs.core.Response.Status.NOT_FOUND;

//TODO add junits
@Path("/transactions")
public class TransactionsResource {

	private final TransactionService service;

	public TransactionsResource() {

		this(TransactionService.getInstance());
	}

	public TransactionsResource(final TransactionService service) {

		this.service = service;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {

		try {
			return Response
					.ok(service.getAll())
					.build();
		} catch (Exception e) {
			return Response.serverError().build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{accountId}")
	public Response getByAccountId(@PathParam("accountId") long accountId) {

		try {
			return Response
					.ok(service.getByAccountId(accountId))
					.build();
		} catch (TransactionNotFoundException e) {
			return Response.status(NOT_FOUND).build();
		} catch (Exception e) {
			return Response.serverError().build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(TransactionDto dto) {

		try {
			TransactionDto createdTransaction = service.create(dto);
			return Response
					.created(URI.create("/transactions/" + createdTransaction.getTransactionId()))
					.entity(createdTransaction)
					.build();
		} catch (Exception e) {
			return Response.serverError().build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response update(@PathParam("id") long id, TransactionDto transactionDto) {

		try {
			return Response
					.ok(service.update(id, transactionDto))
					.build();
		} catch (TransactionNotFoundException e) {
			return Response.status(NOT_FOUND).build();
		} catch (Exception e) {
			return Response.serverError().build();
		}
	}

	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") long id) {

		try {
			service.delete(id);
			return Response
					.ok()
					.build();
		} catch (TransactionNotFoundException e) {
			return Response.status(NOT_FOUND).build();
		} catch (Exception e) {
			return Response.serverError().build();
		}
	}
}
