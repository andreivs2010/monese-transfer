package com.monese.api.resources;

import com.monese.api.dto.AccountDto;
import com.monese.api.dto.TransferDetailsDto;
import com.monese.model.Account;
import com.monese.service.AccountService;
import com.monese.service.TransferService;
import com.monese.service.exception.AmountValidationException;
import com.monese.service.exception.TransferException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

/**
 * Jersey resource for the {@link Account} entity
 *
 * @author acozma
 */
@Path("/accounts")
public class AccountsResource {

	private AccountService accountService = AccountService.getInstance();

	private TransferService transferService = TransferService.getInstance();

	@GET
	@Path("/{accountId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAccount(@PathParam("accountId") String id) throws IOException {

		if (id == null || id.trim().length() == 0) {
			return Response.serverError().entity("UUID cannot be blank").build();
		}
		AccountDto accountDto = accountService.getAccount(Long.parseLong(id));

		if (accountDto == null) {
			return Response.status(Response.Status.NOT_FOUND).entity("Account not found for id: " + id).build();
		}

		String json = accountDto.toJson();//convert entity to json
		return Response.ok(json, MediaType.APPLICATION_JSON).build();

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAccounts() throws IOException {

		List<AccountDto> accountDtos = accountService.getAllAccounts();

		if (accountDtos == null) {
			return Response.status(Response.Status.NOT_FOUND).entity("Accounts not found").build();
		}

		return Response.ok(accountDtos, MediaType.APPLICATION_JSON).build();

	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response storeAccount(AccountDto accountDto) {

		if (accountDto == null) {
			return Response.serverError().entity("Dto cannot be null").build();
		}

		accountService.storeAccount(accountDto);

		return Response.ok().build();
	}

	@POST
	@Path("/{accountId}:transfer")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response transfer(@PathParam("accountId") String accountId, TransferDetailsDto transferDetails) {

		if (transferDetails == null) {
			return Response.status(400).entity("Transfer details object cannot be null").type(MediaType.APPLICATION_JSON).build();
		}
		try {

			transferService.transfer(Long.parseLong(accountId), transferDetails);

		} catch (AmountValidationException exception) {
			return Response.status(400).entity("Amount is not valid").type(MediaType.APPLICATION_JSON).build();
		} catch (TransferException exception) {
			String message = handleException(exception);
			return Response.status(400).entity(message).build();
		} catch (Exception exception) {
			return Response.serverError().entity("Unknown internal error").build();
		}

		return Response.ok().build();
	}

	private String handleException(TransferException exception) {

		TransferException.Reason reason = exception.getReason();

		switch (reason) {
			case SAME_ACCOUNT:
				return "Cannot transfer in the same account";
			case INSUFICIENT_FUNDS:
				return "Transfer declined. Insufficient funds";
			case MISSING_SOURCE_ACCOUNT:
				return "Missing source account";
			case MISSING_TARGET_ACCOUNT:
				return "Missing target account";
				default:
					return "Internal error";
		}
	}

}
