package com.monese.api.resources;

import com.monese.api.dto.ClientDto;
import com.monese.dao.exception.ClientNotFoundException;
import com.monese.model.Client;
import com.monese.service.ClientService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

import static javax.ws.rs.core.Response.Status.NOT_FOUND;

/**
 * Jersey resource for the {@link Client} entity
 *
 * @author acozma
 */
@Path("/clients")
public class ClientsResource {

    private ClientService service;

    public ClientsResource() {

        this(ClientService.getInstance());
    }

    public ClientsResource(final ClientService service) {

        this.service = service;
    }

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response get() {

        try {
            return Response
                    .ok(service.getAllClients())
                    .build();
        } catch (Exception e) {
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getById(@PathParam("id") long id) {

        try {
            return Response.ok(service.getClient(id)).build();
        } catch (Exception e) {
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/{id}/accounts")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccountsOfClientBy(@PathParam("id") long id) {

        try {
            return Response
                    .ok(service.getAccountsOfClientBy(id))
                    .build();
        } catch (Exception e) {
            return Response.serverError().build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(ClientDto clientDto) {

        try {
            ClientDto storedEntity = service.storeClient(clientDto);
            return Response
                    .created(URI.create("/clients/" + storedEntity.getClientId()))
                    .entity(storedEntity)
                    .build();
        } catch (Exception e) {
            return Response.serverError().build();
        }
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") long id, ClientDto clientDto) {

        try {
            return Response
                    .ok(service.updateClient(id, clientDto))
                    .build();
        } catch (Exception e) {
            return Response.serverError().build();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") long id) {

        try {
            service.deleteClient(id);
            return Response
                    .ok()
                    .build();
        } catch (ClientNotFoundException e) {
            return Response
                    .status(NOT_FOUND)
                    .build();
        } catch (Exception e) {
            return Response.serverError().build();
        }
    }

}
