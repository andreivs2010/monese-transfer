package com.monese.api.dto;

import com.monese.model.Client;

import java.util.List;
import java.util.Objects;

/**
 * Data transfer object for {@link Client}
 *
 * @author acozma
 */
public class ClientDto {

    public enum ClientStateDto {

        ACTIVE, INACTIVE, BLACKLISTED
    }

    private Long clientId;

    private String name;

    private ClientStateDto state;

    private String numericPersonalCode;

    private String phoneNumber;

    private String address;

    private String creationDate;

    private String lastModifiedDate;

    private List<Long> accountIds;

    public ClientDto() {

    }

    public ClientDto(Long clientId, String name, ClientStateDto state, String numericPersonalCode, String phoneNumber,
                     String address, String creationDate, String lastModifiedDate, List<Long> accountIds) {

        this.clientId = clientId;
        this.name = name;
        this.state = state;
        this.numericPersonalCode = numericPersonalCode;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.creationDate = creationDate;
        this.lastModifiedDate = lastModifiedDate;
        this.accountIds = accountIds;
    }

    public Long getClientId() {

        return clientId;
    }

    public void setClientId(Long clientId) {

        this.clientId = clientId;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public ClientStateDto getState() {

        return state;
    }

    public void setState(ClientStateDto state) {

        this.state = state;
    }

    public String getNumericPersonalCode() {

        return numericPersonalCode;
    }

    public void setNumericPersonalCode(String numericPersonalCode) {

        this.numericPersonalCode = numericPersonalCode;
    }

    public String getPhoneNumber() {

        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {

        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {

        return address;
    }

    public void setAddress(String address) {

        this.address = address;
    }

    public String getCreationDate() {

        return creationDate;
    }

    public void setCreationDate(String creationDate) {

        this.creationDate = creationDate;
    }

    public String getLastModifiedDate() {

        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {

        this.lastModifiedDate = lastModifiedDate;
    }

    public List<Long> getAccountIds() {

        return accountIds;
    }

    public void setAccountIds(List<Long> accountIds) {

        this.accountIds = accountIds;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ClientDto clientDto = (ClientDto) o;
        return Objects.equals(clientId, clientDto.clientId) &&
                Objects.equals(name, clientDto.name) &&
                state == clientDto.state &&
                Objects.equals(numericPersonalCode, clientDto.numericPersonalCode) &&
                Objects.equals(phoneNumber, clientDto.phoneNumber) &&
                Objects.equals(address, clientDto.address) &&
                Objects.equals(creationDate, clientDto.creationDate) &&
                Objects.equals(lastModifiedDate, clientDto.lastModifiedDate) &&
                Objects.equals(accountIds, clientDto.accountIds);
    }

    @Override
    public int hashCode() {

        return Objects.hash(clientId, name, state, numericPersonalCode, phoneNumber, address, creationDate,
                lastModifiedDate, accountIds);
    }
}
