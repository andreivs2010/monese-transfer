package com.monese.api.dto;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Data transfer object used for the transfer action
 *
 * @author acozma
 */
public class TransferDetailsDto {

	private Long targetAccountId;

	private BigDecimal amount;

	public TransferDetailsDto() {

	}

	public Long getTargetAccountId() {

		return targetAccountId;
	}

	public void setTargetAccountId(Long targetAccountId) {

		this.targetAccountId = targetAccountId;
	}

	public BigDecimal getAmount() {

		return amount;
	}

	public void setAmount(BigDecimal amount) {

		this.amount = amount;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		TransferDetailsDto that = (TransferDetailsDto) o;
		return Objects.equals(targetAccountId, that.targetAccountId) &&
				Objects.equals(amount, that.amount);
	}

	@Override
	public int hashCode() {

		return Objects.hash(targetAccountId, amount);
	}
}
