package com.monese.api.dto;

import com.monese.model.Transaction;

import java.math.BigDecimal;

/**
 * Data transfer object for {@link Transaction}
 *
 * @author acozma
 */
public class TransactionDto {

	public enum TransactionTypeDto {

		TRANSFER
	}

	private Long transactionId;

	private Long parentAccountId;

	private Long parentTransactionLinkId;

	private Long childTransactionLinkId;

	private TransactionTypeDto type;

	private BigDecimal amount;

	private String creationDate;

	public TransactionDto() {

	}

	public TransactionDto(Long transactionId, Long parentAccountId, Long parentTransactionLinkId,
						  Long childTransactionLinkId, TransactionTypeDto type, BigDecimal amount,
						  String creationDate) {

		this.transactionId = transactionId;
		this.parentAccountId = parentAccountId;
		this.parentTransactionLinkId = parentTransactionLinkId;
		this.childTransactionLinkId = childTransactionLinkId;
		this.type = type;
		this.amount = amount;
		this.creationDate = creationDate;
	}

	public Long getTransactionId() {

		return transactionId;
	}

	public void setTransactionId(Long transactionId) {

		this.transactionId = transactionId;
	}

	public Long getParentAccountId() {

		return parentAccountId;
	}

	public void setParentAccountId(Long parentAccountId) {

		this.parentAccountId = parentAccountId;
	}

	public Long getParentTransactionLinkId() {

		return parentTransactionLinkId;
	}

	public void setParentTransactionLinkId(Long parentTransactionLinkId) {

		this.parentTransactionLinkId = parentTransactionLinkId;
	}

	public Long getChildTransactionLinkId() {

		return childTransactionLinkId;
	}

	public void setChildTransactionLinkId(Long childTransactionLinkId) {

		this.childTransactionLinkId = childTransactionLinkId;
	}

	public TransactionTypeDto getType() {

		return type;
	}

	public void setType(TransactionTypeDto type) {

		this.type = type;
	}

	public BigDecimal getAmount() {

		return amount;
	}

	public void setAmount(BigDecimal amount) {

		this.amount = amount;
	}

	public String getCreationDate() {

		return creationDate;
	}

	public void setCreationDate(String creationDate) {

		this.creationDate = creationDate;
	}
}
