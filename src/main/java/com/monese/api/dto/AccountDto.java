package com.monese.api.dto;

import com.monese.model.Account;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * Data transfer object for {@link Account}
 *
 * @author acozma
 */
public class AccountDto {

    public enum AccountStateDto {

        ACTIVE,

        INACTIVE,

        REJECTED
    }

    private Long accountId;

    private BigDecimal balance;

    private AccountStateDto state;

    private Long accountHolderId;

    private List<Long> transactionIds;

    private String creationDate;

    private String lastModifiedDate;

    public Long getAccountId() {

        return accountId;
    }

    public void setAccountId(Long accountId) {

        this.accountId = accountId;
    }

    public BigDecimal getBalance() {

        return balance;
    }

    public void setBalance(BigDecimal balance) {

        this.balance = balance;
    }

    public AccountStateDto getState() {

        return state;
    }

    public void setState(AccountStateDto state) {

        this.state = state;
    }

    public Long getAccountHolderId() {

        return accountHolderId;
    }

    public void setAccountHolderId(Long accountHolderId) {

        this.accountHolderId = accountHolderId;
    }

    public List<Long> getTransactionIds() {

        return transactionIds;
    }

    public void setTransactionIds(List<Long> transactionIds) {

        this.transactionIds = transactionIds;
    }

    public String getCreationDate() {

        return creationDate;
    }

    public void setCreationDate(String creationDate) {

        this.creationDate = creationDate;
    }

    public String getLastModifiedDate() {

        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {

        this.lastModifiedDate = lastModifiedDate;
    }

    public String toJson() throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        return mapper.writeValueAsString(this);
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        AccountDto that = (AccountDto) o;
        return Objects.equals(accountId, that.accountId) &&
                Objects.equals(balance, that.balance) &&
                state == that.state &&
                Objects.equals(accountHolderId, that.accountHolderId) &&
                Objects.equals(transactionIds, that.transactionIds) &&
                Objects.equals(creationDate, that.creationDate) &&
                Objects.equals(lastModifiedDate, that.lastModifiedDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(accountId, balance, state, accountHolderId, transactionIds, creationDate, lastModifiedDate);
    }
}
