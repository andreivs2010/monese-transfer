package com.monese;

import com.monese.model.Client;

import javax.persistence.EntityManager;
import java.util.Iterator;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App {

	private static EntityManager entityManager = EntityManagerUtils.getEntityManager();

	public static void main(String[] args) throws Exception {

		Client client = createClient("Andrei");
		Client client2 = createClient("Andra");
		Client client3 = createClient("Andreea");
		Client client4 = createClient("Matei");
		Client client5 = createClient("Mara");

		listClients();

		removeClient(client);

		listClients();

	}

	private static void removeClient(Client client) {
		entityManager.getTransaction().begin();
		entityManager.remove(client);
		entityManager.getTransaction().commit();
	}

	private static Client createClient(String clientName) {
		Client client = new Client();
		try {
			entityManager.getTransaction().begin();
			client.setName(clientName);
			client = entityManager.merge(client);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		}

		return client;
	}

	public static void listClients() {
		try {
			entityManager.getTransaction().begin();
			@SuppressWarnings("unchecked")
			List<Client> clients = entityManager.createQuery("from Client").getResultList();
			for (Iterator<Client> iterator = clients.iterator(); iterator.hasNext();) {
				Client client = (Client) iterator.next();
				System.out.println(client.getName());
			}
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		}
	}
}
