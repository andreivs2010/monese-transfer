package com.monese.dao;

import com.monese.dao.exception.EntityNotFoundException;
import com.monese.model.Client;
import com.monese.model.HasId;

import java.util.List;

/**
 * Base interface for CRUD operations on entities
 *
 * @author acozma
 */
public interface EntityDao<E extends HasId> {

    /**
     * Store new entity or modify existing one
     *
     * @param entity
     * 		- new entity or current entity
     *
     * @return the entity
     */
    void storeAndTouchEntity(E entity);

    /**
     * Store new entity or modify existing one
     *
     * @param entity
     * 		- new entity or current entity
     *
     * @return the entity
     */
    void storeEntity(E entity);

    /**
     * Update and touch existing entity
     *
     * @param entity
     * 		-  current entity
     *
     * @return the entity
     */
    E updateAndTouchEntity(E entity);

    /**
     * Update existing entity
     *
     * @param entity
     * 		-  current entity
     *
     * @return the entity
     */
    E updateEntity(E entity);

    /**
     * Retrieve the entity instance with given id
     *
     * @param id
     * 		- given id
     *
     * @return {entity instance
     */
    E getEntity(Long id);

    /**
     * Retrieve the list of entities with given ids
     *
     * @param idList
     * 		- list of entity ids
     *
     * @return a list of {@link Client} instances with given ids
     */
    List<E> getEntities(List<Long> idList);

    /**
     * Retrieve the list of all entities
     *
     * @return the list of all entities
     */
    List<E> getAllEntities();

    /**
     * Remove the entity with given id
     *
     * @param id
     * 		- {@link Client} id
     */
    void delete(Long id) throws EntityNotFoundException;

}
