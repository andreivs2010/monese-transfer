package com.monese.dao.impl;

import com.monese.dao.exception.AccountNotFoundException;
import com.monese.dao.exception.EntityNotFoundException;
import com.monese.model.Account;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.function.Function;

/**
 * Data access layer for CRUD operations on {@link Account} entities
 *
 * @author acozma
 */
public class AccountDaoImpl extends AbstractEntityDaoImpl<Account> {

	private AccountDaoImpl() {

	}

	private static class Singleton {

		private final static AccountDaoImpl INSTANCE = new AccountDaoImpl();
	}

	public static AccountDaoImpl getInstance() {

		return Singleton.INSTANCE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Function<EntityManager, Account> createFinder(final Long id) {

		return em -> em.find(Account.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String createQueryForIds(final List<Long> ids) {

		return "from Account where id in (?1)";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String createQueryForAll() {

		return "from Account ";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EntityNotFoundException createEntityNotFoundException() {

		return new AccountNotFoundException();
	}

}
