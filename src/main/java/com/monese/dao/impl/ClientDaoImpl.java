package com.monese.dao.impl;

import com.monese.dao.exception.ClientNotFoundException;
import com.monese.dao.exception.EntityNotFoundException;
import com.monese.model.Client;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.function.Function;

/**
 * Data access layer for CRUD operations on {@link Client} entities
 *
 * @author acozma
 */
public class ClientDaoImpl extends AbstractEntityDaoImpl<Client> {

	private ClientDaoImpl() {

	}

	private static class Singleton {
		private final static ClientDaoImpl INSTANCE = new ClientDaoImpl();
	}

	public static ClientDaoImpl getInstance() {

		return Singleton.INSTANCE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Function<EntityManager, Client> createFinder(final Long id) {

		return em -> em.find(Client.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String createQueryForIds(final List<Long> ids) {

		return "from Client where id in (?1)";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String createQueryForAll() {

		return "from Client ";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EntityNotFoundException createEntityNotFoundException() {

		return new ClientNotFoundException();
	}
}
