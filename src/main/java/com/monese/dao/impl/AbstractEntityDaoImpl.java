package com.monese.dao.impl;

import com.monese.EntityManagerUtils;
import com.monese.dao.EntityDao;
import com.monese.dao.exception.EntityNotFoundException;
import com.monese.model.HasId;
import com.monese.model.IsTouchable;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Base class for CRUD operations on entities
 *
 * @author acozma
 */
public abstract class AbstractEntityDaoImpl<E extends HasId & IsTouchable> implements EntityDao<E> {

    private <T extends Throwable> void consumeWithinTransactional(final Consumer<EntityManager> consumer,
                                                                  Class<T> exceptionClass) {

        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            consumer.accept(entityManager);
            entityManager.getTransaction().commit();

        } catch (Exception e) {
            if (exceptionClass.isInstance(e)) {
                entityManager.getTransaction().rollback();
            } else {
                throw e;
            }
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void storeAndTouchEntity(E entity) {

        entity.touch();
        storeEntity(entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void storeEntity(final E entity) {

        consumeWithinTransactional(em -> em.persist(entity), Exception.class);

    }

    protected <R> R executeWithinTransactionalContext(Function<EntityManager, R> function) {

        EntityManager entityManager = EntityManagerUtils.getEntityManager();
        R result = null;
        try {
            entityManager.getTransaction().begin();
            result = function.apply(entityManager);
            entityManager.getTransaction().commit();

        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E updateAndTouchEntity(final E entity) {

        entity.touch();
        return updateEntity(entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E updateEntity(final E entity) {

        return executeWithinTransactionalContext(em -> em.merge(entity));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E getEntity(Long id) {

        Function<EntityManager, E> finder = createFinder(id);
        return executeWithinTransactionalContext(finder);

    }

    /**
     * Generates a {@link Function} used to fetch an entity with given id using applied persistence manager
     *
     * @param id
     * 		given entity id
     *
     * @return the function
     */
    public abstract Function<EntityManager, E> createFinder(final Long id);

    /**
     * {@inheritDoc}
     */
    public List<E> getEntities(List<Long> ids) {

        Function<EntityManager, List<E>> finder = em -> em.createQuery(createQueryForIds(ids))
                .setParameter(1, ids)
                .getResultList();
        return executeWithinTransactionalContext(finder);
    }

    /**
     * Generates a query for fetching entities with given ids
     *
     * @param ids
     * 		given ids
     *
     * @return the query string used for fetching entities with given ids
     */
    public abstract String createQueryForIds(final List<Long> ids);

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> getAllEntities() {

        Function<EntityManager, List<E>> finder = em -> em.createQuery(createQueryForAll())
                .getResultList();
        return executeWithinTransactionalContext(finder);
    }

    /**
     * Generates a query for fetching all entities from a table
     *
     * @return the query string used for fetching entities
     */
    public abstract String createQueryForAll();

    /**
     * {@inheritDoc}
     */
    public void delete(final Long id) throws EntityNotFoundException {

        Consumer<EntityManager> remover = em -> {
            E entity = createFinder(id).apply(em);

            if (entity == null) {
                throw createEntityNotFoundException();
            }

            em.remove(entity);
        };

        consumeWithinTransactional(remover, IllegalStateException.class);
    }

    /**
     * @return the {@link EntityNotFoundException} for current implementation
     */
    public abstract EntityNotFoundException createEntityNotFoundException();
}
