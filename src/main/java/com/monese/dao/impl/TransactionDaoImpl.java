package com.monese.dao.impl;

import com.monese.dao.exception.EntityNotFoundException;
import com.monese.dao.exception.TransactionNotFoundException;
import com.monese.model.Transaction;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.function.Function;

/**
 * Data access layer for CRUD operations on {@link Transaction} entities
 *
 * @author acozma
 */
public class TransactionDaoImpl extends AbstractEntityDaoImpl<Transaction> {

    private TransactionDaoImpl() {

    }

    private static class Singleton {
        private final static TransactionDaoImpl INSTANCE = new TransactionDaoImpl();
    }

    public static TransactionDaoImpl getInstance() {

        return Singleton.INSTANCE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Function<EntityManager, Transaction> createFinder(final Long id) {

        return em -> em.find(Transaction.class, id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createQueryForIds(final List<Long> ids) {

        return "from Transaction where id in (?1)";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String createQueryForAll() {

        return "from Transaction ";
    }

    private String createQueryForAccountId() {

        return "from Transaction where accountId=?1";
    }

    public List<Transaction> getByAccountId(Long accountId) {

        Function<EntityManager, List<Transaction>> finder = em -> em.createQuery(createQueryForAccountId())
                .setParameter(1, accountId)
                .getResultList();

        return executeWithinTransactionalContext(finder);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityNotFoundException createEntityNotFoundException() {

        return new TransactionNotFoundException();
    }

}
