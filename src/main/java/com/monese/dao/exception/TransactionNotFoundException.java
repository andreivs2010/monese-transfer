package com.monese.dao.exception;

import com.monese.model.Transaction;

/**
 * Checked exception which should be thrown when trying to delete inexisting {@link Transaction}
 *
 * @author acozma
 */
public class TransactionNotFoundException extends EntityNotFoundException {

}
