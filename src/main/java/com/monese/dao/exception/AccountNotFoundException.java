package com.monese.dao.exception;

import com.monese.model.Account;

/**
 * Checked exception which should be thrown when trying to delete inexisting {@link Account}
 *
 * @author acozma
 */
public class AccountNotFoundException extends EntityNotFoundException {

}
