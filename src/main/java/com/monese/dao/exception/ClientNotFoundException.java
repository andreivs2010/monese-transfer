package com.monese.dao.exception;

import com.monese.model.Client;

/**
 * Checked exception which should be thrown when trying to delete inexisting {@link Client}
 *
 * @author acozma
 */
public class ClientNotFoundException extends EntityNotFoundException {

}
