package com.monese.dao.exception;

/**
 * Checked exception which should be thrown when trying to delete inexisting entity
 *
 * @author acozma
 */
public class EntityNotFoundException extends RuntimeException {

}
