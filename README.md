# monese-transfer


##Description
Application run on an embedded Jetty server 
which provides Jersey endpoints for managing 
an end-to-end transfer between client accounts

##How to use
In order to start the application, run JerseyApplication. It is also set as a main class in maven pom.xml (executable jar with dependencies).
Once the server is up and running and it even creates demo data by provisioning clients, accounts and transactions.

##Transfer example:

The endpoint consumes a data transfer object in JSON format and returns 200 OK status if valid

* targetAccountId - the id for the target account
* amount - the transferred amount

i.e with curl:

curl -d '{"targetAccountId":4,"amount":3}' -x POST http://localhost:8080/api/accounts/1:transfer  -H "Content-Type: application/json"